// NAME: quick.c
// TIME OF CREATION: 2022-06-08 20:20:13
// AUTHOR:
// DESCRIPTION: Sorting visualiser

#include "all.h"

// For DrawHelpMessage and general font rendering (immf_*)
// Keeps the font size consistent regardless of how glOrtho is set up
#define VIRTUAL_WIDTH 800
#define VIRTUAL_HEIGHT 600

// For glOrtho
// Controls with which resolution the bars are visualized
#define ORTHO_WIDTH 16000
#define ORTHO_HEIGHT 16000

#define COLOR_WHITE (color3f){1, 1, 1}
#define COLOR_RED (color3f){1, 0, 0}
#define BG_COLOR 0.120, 0.120, 0.120

#define RANDOMIZE_DELAY_DEFAULT 1000000

#define min(a, b) ((a) < (b) ? (a) : (b))

#define ArrayCount(a_) (sizeof(a_) / sizeof(a_)[0])

#define PI 3.141592654

#define UNUSED(v) (void)((v))

/**********************************
* Types
**********************************/
#pragma pack(push, 1)
typedef struct
{
	f32 r;
	f32 g;
	f32 b;
} color3f;

typedef struct
{
	f32 x;
	f32 y;

	color3f Color;
} quad_vert;
#pragma pack(pop)

typedef struct
{
	f32 x;
	f32 y;
} v2f;

typedef struct
{
	quad_vert v[4];
} quad;

typedef struct
{
	v2f v[4];
} quad_basic;

typedef struct
{
	quad *Quads;
	quad_basic *Top;
	u32 count;
} quads;

typedef struct
{
	u32 min;
	u32 max;
	u32 *values;
	u32 count;
} random_values;

typedef struct
{
	quads *Quads;
	random_values *RandomValues;
} sort_data;

// Draws quads from this range in our main quad array
// Used for things like visualising the quick sort pivot range
typedef struct
{
	u32 begin;
	u32 end;
} auxillary_quad_data;

typedef struct
{
	bool displayHelp;
	bool soundIsMuted;
} options;

typedef struct
{
	int windowedWidth;
	int windowedHeight;
	int xpos;
	int ypos;
} client_window;



typedef struct sort_context sort_context;
typedef void (*pfnSort)(sort_context *Context);
typedef void (*pfnRand) (random_values *Values);

typedef enum
{
	ST_QUICK,
	ST_SHELL_V1,
	ST_SHELL_V2,
	ST_QUICK_SHELL,
	ST_MERGE_INPLACE,
	ST_MERGE_BOTTOM_UP,
	ST_MERGE_TOP_DOWN,
	ST_RADIX65536,
	ST_RADIX256,
	ST_RADIX10,
	ST_RADIX4,
	ST_RADIX3,
	ST_RADIX2,
	ST_INSERTION,
	ST_BUBBLE,
	ST_COCKTAIL,
	ST_SELECTION,
	ST_BOGO,
	ST_COUNTING,
	ST_SHUFFLE,

	ST_TOTAL
} sort_type;

// This is a global, but it's here for ease of tracking
const char *GlobalSortDescriptions[ST_TOTAL] =
{
	[ST_SHUFFLE]  			= "Shuffle", /* Special, the randomization step */
	[ST_QUICK] 				= "Quick sort",
	[ST_SHELL_V1] 			= "Shell sort",
	[ST_SHELL_V2] 			= "Shell sort (improved gaps)",
	[ST_QUICK_SHELL] 		= "Quick/Shell hybrid sort",
	[ST_MERGE_INPLACE] 		= "Merge sort (in place)",
	[ST_MERGE_BOTTOM_UP] 	= "Merge sort (bottom up)",
	[ST_MERGE_TOP_DOWN] 	= "Merge sort (top down)",
	[ST_RADIX65536] 		= "Radix sort (base 65536)",
	[ST_RADIX256] 			= "Radix sort (base 256)",
	[ST_RADIX10] 			= "Radix sort (base 10)",
	[ST_RADIX4] 			= "Radix sort (base 4)",
	[ST_RADIX3] 			= "Radix sort (base 3)",
	[ST_RADIX2] 			= "Radix sort (base 2)",
	[ST_BUBBLE] 			= "Bubble sort",
	[ST_INSERTION] 			= "Insertion sort",
	[ST_COCKTAIL] 			= "Cocktail sort",
	[ST_SELECTION] 			= "Selection sort",
	[ST_BOGO] 				= "Bogo sort",
	[ST_COUNTING] 			= "Counting sort",
};


typedef enum
{
	RND_RANDOM,
	RND_REVERSE,
	RND_ALMOST_SORTED,
	RND_ALMOST_SORTED_AT_BEG,
	RND_ALMOST_SORTED_AT_END,

	RND_FUNC_TOTAL
} rand_func;

typedef struct
{
	u64 delayNs;
	sort_type SortType;
	u32 min;
	u32 max;
	rand_func RandFunc;
	u32 genTimeout;
} sort_config;



struct sort_context
{
	sort_data *Data;

	sort_config *SortConfig;
	u32 sortConfigCount;
	sort_type ActiveSort;
	rand_func ActiveRandFunc;

	u64 delayNs;
	u64 accesses;
	u64 comparisons;

	u32 genIndex;

	f32 secondsForCurrentRun;

	u32 currentIteration;
	pthread_t threadHandle;

	auxillary_quad_data AuxSwap;
	auxillary_quad_data AuxShellRange;
	auxillary_quad_data AuxQuickPivot;

	bool DrawSwaps;
	bool DrawShellRange;
	bool DrawQuickPivot;
};

typedef struct
{
	char *memory;
	intptr_t used;
	intptr_t capacity;
} memory_region;

typedef enum
{
	SPM_CANCEL,
	SPM_RESTART,
	SPM_SORT_PREV,
	SPM_SORT_NEXT,
} sort_proc_message;

#define SORT_MESSAGE_QUEUE_STACK_SIZE 64
typedef struct
{
	sort_proc_message Messages[SORT_MESSAGE_QUEUE_STACK_SIZE];
	int sp;
} sort_proc_message_queue;






/**********************************
* Function declarations
**********************************/
void SortContextSleep(sort_context *Context);
void SortContextSleepNoTimeout(sort_context *Context);
void ContextAuxillaryStateReset(sort_context *Context);
void SleepSec(unsigned int seconds);
void SleepNano(u64 nanoseconds);
void SleepMilli(double milliseconds);
void *SortProc(void *arg);
void SortProcMesageSend(sort_proc_message Message);
void MemZero(void *dst, size_t bytes);
u64 MapElementCountToDelay(u64 elements, u64 delayNs);


/**********************************
* Globals
**********************************/
xs_state32 GlobalRandState = {.state = 69};
bool GlobalRunning = 1;
bool ThreadTimeToQuit = 0;
bool TimeToSortAgain = 0;
bool SortingHasFinished = 0;
pthread_mutex_t SortLock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t SortCond = PTHREAD_COND_INITIALIZER;
color3f GlobalBarTopColor = {0};
bool WindowIsFullscreen = true;

memory_region GlobalMemoryRegionPermanent = {0};
memory_region GlobalMemoryRegionScratch = {0};

pthread_mutex_t SortProcMessageLock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t SortProcMessageCond = PTHREAD_COND_INITIALIZER;
sort_proc_message_queue GlobalSortProcMessageQueue = {0};


// This needs to be a global for pthread_join
sort_context *pGlobalSortContext;

options Options = {0};




pthread_cond_t ClockProcCond = PTHREAD_COND_INITIALIZER;
bool SortIsRunning = 0;

GLFWmonitor *GlobalMonitor;
client_window GlobalClientWindow;

#include "r_gl.c"
#include "snd.c"
#include "platform.c"

/**********************************
* Function definitions
**********************************/

void *MemoryRegionAlloc(memory_region *Region, size_t bytes)
{
	assert(Region->memory != NULL);
	assert(bytes + Region->used <= Region->capacity);
	char *Result = &Region->memory[Region->used];
	Region->used += bytes;
	return Result;
}

void *ScratchAlloc(size_t bytes)
{
	memory_region *Region = &GlobalMemoryRegionScratch;
	return MemoryRegionAlloc(Region, bytes);
}

void ScratchClear(void)
{
	GlobalMemoryRegionScratch.used = 0;
}

#define Kilobytes(v_) ((v_) * 1024ULL)
#define Megabytes(v_) (Kilobytes((v_)) * 1024ULL)

void MemoryRegionsInit()
{
	size_t capPermanent = Megabytes(64);
	size_t capScratch = Megabytes(64);

	// @leakleak!
	// This is the only allocation we explicitly perform in the entire program (not including thirdparty code)

 	char *memory = malloc(capScratch + capPermanent);
	assert(memory != NULL);

	GlobalMemoryRegionPermanent = (memory_region){0};
	GlobalMemoryRegionScratch = (memory_region){0};

	GlobalMemoryRegionPermanent.memory = memory;
	GlobalMemoryRegionScratch.memory = &memory[capPermanent];

	GlobalMemoryRegionScratch.capacity = capScratch;
	GlobalMemoryRegionPermanent.capacity = capPermanent;
}

void *PermanentAlloc(size_t bytes)
{
	memory_region *Region = &GlobalMemoryRegionPermanent;
	return MemoryRegionAlloc(Region, bytes);
}

void PermanentClear()
{
	GlobalMemoryRegionPermanent.used = 0;
}

quads *QuadsCreate(u32 numQuads)
{
	quads *Result;

	Result = PermanentAlloc(sizeof *Result);
	Result->Quads = PermanentAlloc(sizeof *Result->Quads * numQuads);
	Result->Top = PermanentAlloc(sizeof *Result->Top * numQuads);
	Result->count = numQuads;

	return Result;
}

u32 RandInclusive(u32 min, u32 max)
{
	assert(min < max);
	return xs_range_in_u32(&GlobalRandState, min, max);
}

void RndInit(random_values *Values)
{
	for(u32 i = 0; i < Values->count; i++)
	{
		u32 val = i + 1;
		Values->values[i] = val;
	}
}

static void RndRandom(random_values *Values)
{
	RndInit(Values);

	for(u32 i = 0; i < Values->count; i++)
	{
		u32 pos = RandInclusive(0, Values->count - 1);
		u32 tmp = Values->values[i];
		Values->values[i] = Values->values[pos];
		Values->values[pos] = tmp;
	}
}

static void RndReverse(random_values *Values)
{
	RndInit(Values);

	for(u32 i = 0; i <= Values->count / 2; i++)
	{
		u32 tmp = Values->values[i];
		Values->values[i] = Values->values[Values->count - i - 1];
		Values->values[Values->count - i - 1] = tmp;
	}
}

static void RndAlmostSorted(random_values *Values)
{
	RndInit(Values);

	for(u32 i = 0; i < Values->count / 16; i++)
	{
		u32 pos1 = RandInclusive(0, Values->count - 1);
		u32 pos2 = RandInclusive(0, Values->count - 1);
		u32 tmp = Values->values[pos1];
		Values->values[pos1] = Values->values[pos2];
		Values->values[pos2] = tmp;
	}
}

static void RndAlmostSortedAtBeg(random_values *Values)
{
	RndInit(Values);

	u32 lowerbound = 0;
	u32 upperbound = Values->count / 128;

	for(u32 i = lowerbound; i < upperbound; i++)
	{
		u32 pos = RandInclusive(lowerbound, upperbound);
		u32 tmp = Values->values[i];
		Values->values[i] = Values->values[pos];
		Values->values[pos] = tmp;
	}
}

static void RndAlmostSortedAtEnd(random_values *Values)
{
	RndInit(Values);
	u32 lowerbound = Values->count - (Values->count / 128);
	u32 upperbound = Values->count > 0 ? Values->count - 1: 0;
	for(u32 i = upperbound; i > lowerbound; i--)
	{
		u32 pos = RandInclusive(lowerbound, upperbound);
		u32 tmp = Values->values[i];
		Values->values[i] = Values->values[pos];
		Values->values[pos] = tmp;
	}
}

void RandomValuesRandomize(random_values *Values)
{
#if 0
	// Not strictly from 0 to Values->count, there may be some duplicates or missing numbers
	for(u32 i = 0; i < Values->count; i++)
	{
		u32 val = RandInclusive(Values->min, Values->max - 1);
		Values->values[i] = val;
	}
#else
	for(u32 i = 0; i < Values->count; i++)
	{
		u32 val = i + 1;
		Values->values[i] = val;
	}

	// for(int c = 0; c < 10; c++)
	{
#if 1
		for(u32 i = 0; i < Values->count; i++)
		{
			u32 pos = RandInclusive(0, Values->count - 1);
			u32 tmp = Values->values[i];
			Values->values[i] = Values->values[pos];
			Values->values[pos] = tmp;
		}
#else
		// reverse
		for(u32 i = 0; i <= Values->count / 2; i++)
		{
			u32 pos = RandInclusive(0, Values->count - 1);
			u32 tmp = Values->values[i];
			Values->values[i] = Values->values[Values->count - i - 1];
			Values->values[Values->count - i - 1] = tmp;
		}
#endif

	}
#endif
}

random_values *RandomValuesCreate(u32 min, u32 max, u32 count)
{
	assert(count > 0);
	random_values *Result = PermanentAlloc(sizeof *Result);
	Result->values = PermanentAlloc(sizeof *Result->values * count);

	Result->min = min;
	Result->max = max;
	Result->count = count;
	return Result;
}

random_values *RandomValuesCreateAndRandomize(u32 min, u32 max, u32 count)
{
	assert(count > 0);
	random_values *Result = RandomValuesCreate(min, max, count);
	RandomValuesRandomize(Result);
	return Result;
}

void QuadsMapToRandomValues(quads *Quads, random_values *Values, bool shouldDelay, const u64 delayNs, u64 *accesses)
{
	assert(Quads->count == Values->count);

	u32 count = Quads->count;

	// Shift everything over to the right and lower the top
	// We originally hardcodeed these offsets with an ortho of 800x600, so we scale the offsets by this amount for consistent appearances
	f32 xOffset = 25.0 * (ORTHO_WIDTH / 800.0);
	f32 yOffset = 25.0 * (ORTHO_HEIGHT / 600.0);
	f32 topOffset = 3.0 * (ORTHO_HEIGHT / 600.0);


	f32 w = (f32)(ORTHO_WIDTH - xOffset) / (f32)count;

	f32 range = Values->max - Values->min;

	float frequency =  (20.0 / PI) / (float)range;
	// 20 seems to be the correct ratio, but why 20?
	// We got this number by trial and error

	for(u32 i = 0; i < count; i++)
	{
		u32 val = Values->values[i];
		f32 percentage = (float)(val - Values->min) / range;

		f32 h = (f32)(ORTHO_HEIGHT - yOffset) * percentage;

		float x0, x1, y0, y1;

		x0 = w * i + xOffset;
		x1 = x0 + w;
		y0 = 0;
		y1 = h;

		float r = cos((count - val) * frequency + 0); 				// Phase shift 0 deg
		float g = cos((count - val) * frequency + (2 * PI / 3)); 	// Phase shift 120 deg
		float b = cos((count - val) * frequency + (4 * PI / 3)); 	// Phase shift 240 deg

		// Normalize
		r = (r / 2.0) + 0.5;
		g = (g / 2.0) + 0.5;
		b = (b / 2.0) + 0.5;

		color3f Color = {r, g, b};

		quad *Q = &Quads->Quads[i];
		quad_basic *Qb = &Quads->Top[i];

		// Bottom left
		Q->v[0].x = x0;
		Q->v[0].y = y0;
		Q->v[0].Color = Color;

		// Top left
		Q->v[1].x = x0;
		Q->v[1].y = y1;
		Q->v[1].Color = Color;

		// Top right
		Q->v[2].x = x1;
		Q->v[2].y = y1;
		Q->v[2].Color = Color;

		// Bottom right
		Q->v[3].x = x1;
		Q->v[3].y = y0;
		Q->v[3].Color = Color;

		// Top of the quad (color band)
		// We offset them by 'topOffset' pixels
		// The reason we have an extra array for the top is so that we can
		// send it as a vertex buffer to OpenGL via glVertexPointer
		// This speeds up drawing significantly
		// Color is determined during glDrawArrays
		//----------------------------------------------------------------
		// Bottom left
		Qb->v[0].x = x0;
		Qb->v[0].y = y1 - topOffset; //  was -3

		// Top left
		Qb->v[1].x = x0;
		Qb->v[1].y = y1;

		// Top right
		Qb->v[2].x = x1;
		Qb->v[2].y = y1;

		// Bottom right
		Qb->v[3].x = x1;
		Qb->v[3].y = y1 - topOffset; // was -3

		snd_PitchSet(SND_SOURCE_RIGHT, val);

		if(shouldDelay && accesses != NULL)
		{
			SleepNano(delayNs);
			*accesses = *accesses + 1;
		}
	}
}



void QuadsCompletionColorSet(sort_context *Context)
{
	const color3f White = {.r = 1, .g = 1, .b = 1};
	quads *Quads = Context->Data->Quads;
	for(u32 i = 0; i < Quads->count; i++)
	{
		quad *Q = &Quads->Quads[i];

		Q->v[0].Color = White;
		Q->v[1].Color = White;
		Q->v[2].Color = White;
		Q->v[3].Color = White;

		u32 v = Context->Data->RandomValues->values[i];
		snd_PitchSet(SND_SOURCE_RIGHT, v);

		// We sleep for a duration proportional to the element count
		u64 count = Context->Data->RandomValues->count;
		u64 delay = MapElementCountToDelay(count, RANDOMIZE_DELAY_DEFAULT);
		Context->delayNs = delay;
		SleepNano(delay);
		// SortContextSleepNoTimeout(Context);
		// SortContextSleep(Context);
	}
}

// Must only be called from the main thread!
void SortProcShutdownAndJoin()
{
	// We have a bug here, but we don't know why
	void *ret;
	SortProcMesageSend(SPM_CANCEL);
	int rc = pthread_join(pGlobalSortContext->threadHandle, &ret);
	// assert(ret == PTHREAD_CANCELED);
}


void ContextReset(sort_context *Context)
{
	Context->accesses = 0;
	Context->comparisons = 0;
	Context->genIndex = 0;
	ContextAuxillaryStateReset(Context);
}


void CbResize(GLFWwindow *Window, int x, int y)
{
	glViewport(0, 0, x, y);
}


#define KEY_PRESS(k) if(key == GLFW_##k && action == GLFW_PRESS)
void CbKeys(GLFWwindow *Window, int key, int scancode, int action, int mod)
{
	// Quit
	KEY_PRESS(KEY_Q)
	{
		GlobalRunning = 0;
	}

	KEY_PRESS(KEY_ESCAPE)
	{
		GlobalRunning = 0;
	}

	// Restart current sort
	KEY_PRESS(KEY_UP)
	{
		SortProcShutdownAndJoin();
		SortProcMesageSend(SPM_RESTART);
	}

	// Next sort
	KEY_PRESS(KEY_RIGHT)
	{
		SortProcShutdownAndJoin();
		SortProcMesageSend(SPM_SORT_NEXT);
		SortProcMesageSend(SPM_RESTART);
	}

	// Previous sort
	KEY_PRESS(KEY_LEFT)
	{
		SortProcShutdownAndJoin();
		SortProcMesageSend(SPM_SORT_PREV);
		SortProcMesageSend(SPM_RESTART);
	}

	// Toggle full screen
	KEY_PRESS(KEY_F)
	{


		WindowIsFullscreen = !WindowIsFullscreen;

		if(WindowIsFullscreen)
		{
			const GLFWvidmode *VidMode = glfwGetVideoMode(GlobalMonitor);
			glfwGetWindowSize(Window, &GlobalClientWindow.windowedWidth, &GlobalClientWindow.windowedHeight);
			glfwGetWindowPos(Window, &GlobalClientWindow.xpos, &GlobalClientWindow.ypos);
			glfwSetWindowMonitor(Window, GlobalMonitor, 0, 0, VidMode->width, VidMode->height, GLFW_DONT_CARE);
		}
		else
		{
			glfwSetWindowMonitor(Window, 0,
					GlobalClientWindow.xpos, GlobalClientWindow.ypos,
					GlobalClientWindow.windowedWidth, GlobalClientWindow.windowedHeight,
					GLFW_DONT_CARE);
		}
	}

	if((key == GLFW_KEY_H || key == GLFW_KEY_F1 || key == GLFW_KEY_SLASH) && action == GLFW_PRESS)
	{
		switch(key)
		{
			case GLFW_KEY_SLASH:
				if(key == GLFW_KEY_SLASH && mod != GLFW_MOD_SHIFT) break;
			default:
				snd_Play(SND_SOURCE_HELP);
				Options.displayHelp = !Options.displayHelp;
		}
	}

	// Mute, or unmute
	KEY_PRESS(KEY_M)
	{
		if(Options.soundIsMuted)
		{
			snd_UnmuteAll();
		}
		else
		{
			snd_MuteAll();
		}
		Options.soundIsMuted = !Options.soundIsMuted;
	}
}


int RealGlyphDim()
{
	return (float)12 * ((float)VIRTUAL_WIDTH / 640.0);
}

void DrawHelpMessage()
{
	if(!Options.displayHelp) return;

	int dim = 12;// RealGlyphDim();
	float w = 350;
	float h = 200 + dim; // + 20 for each line
	float x = ((float)VIRTUAL_WIDTH / 2) - (w / 2);
	float y = ((float)VIRTUAL_HEIGHT / 2) - (h / 2);

	float off = 20;

	u32 bgColor = 0xFF000000;
	u32 textColor = 0xFFFFFFFF;
	u32 borderColor = 0xFFFFFFFF;

	// We want our textbox to look the same no matter what the ORTHO_HEIGHT and ORTHO_HEIGHT values are
	const float a = 2.0 / VIRTUAL_WIDTH;
	const float f = 2.0 / VIRTUAL_HEIGHT;

	const float m[16] =
	{
		a, 0, 0, 0,
		0, -f, 0, 0,
		0, 0, 1, 0,
		-1, 1, 1, 1,
	};

	glPushMatrix();
	glLoadMatrixf(m);

	// Border
	gl_DrawQuad(x - (off / 2), y - (off / 2), w + off, h + off, borderColor);
	// Background
	gl_DrawQuad(x, y, w, h, bgColor);

	glPopMatrix();


	// Text
	immf_PushAttrib(IMM_ATTRIB_SPACING_VERT);
	immf_FontSpacingVert(8);
	immf_DrawString(x + dim, y + dim, dim, dim, textColor,
			"%s",
			"Controls\n"
			"Next sort:         RIGHT\n"
			"Previous sort:     LEFT\n"
			"Repeat sort:       UP\n"
			"(Un)mute Audio:    M\n"
			"Toggle Fullscreen: F\n"
			"Toggle Help:       H / F1\n"
			"Quit:              Q / ESC\n"
			"\n"
			"Written in 2022.06 by Z"
			);

	immf_PopAttrib();
}

void QuadSwap(quad *Q0, quad *Q1)
{
	float x0[4];
	float x1[4];

	// Cache coordinates, swap, and write them back
	// Since we are swapping everything except for X, this is easier to write

	// Q0
	x0[0] = Q0->v[0].x;
	x0[1] = Q0->v[1].x;
	x0[2] = Q0->v[2].x;
	x0[3] = Q0->v[3].x;

	// Q1
	x1[0] = Q1->v[0].x;
	x1[1] = Q1->v[1].x;
	x1[2] = Q1->v[2].x;
	x1[3] = Q1->v[3].x;

	quad tmp = *Q0;
	*Q0 = *Q1;
	*Q1 = tmp;

	Q0->v[0].x = x0[0];
	Q0->v[1].x = x0[1];
	Q0->v[2].x = x0[2];
	Q0->v[3].x = x0[3];

	Q1->v[0].x = x1[0];
	Q1->v[1].x = x1[1];
	Q1->v[2].x = x1[2];
	Q1->v[3].x = x1[3];
}

void TopSwap(quad_basic *Qb0, quad_basic *Qb1)
{
	float x0[4];
	float x1[4];


	// Qb0
	x0[0] = Qb0->v[0].x;
	x0[1] = Qb0->v[1].x;
	x0[2] = Qb0->v[2].x;
	x0[3] = Qb0->v[3].x;

	// Qb1
	x1[0] = Qb1->v[0].x;
	x1[1] = Qb1->v[1].x;
	x1[2] = Qb1->v[2].x;
	x1[3] = Qb1->v[3].x;

	quad_basic tmp = *Qb0;
	*Qb0 = *Qb1;
	*Qb1 = tmp;

	Qb0->v[0].x = x0[0];
	Qb0->v[1].x = x0[1];
	Qb0->v[2].x = x0[2];
	Qb0->v[3].x = x0[3];

	Qb1->v[0].x = x1[0];
	Qb1->v[1].x = x1[1];
	Qb1->v[2].x = x1[2];
	Qb1->v[3].x = x1[3];
}


void QuadColorSet1(quad *Quad, color3f Color)
{
	Quad->v[0].Color = Color;
	Quad->v[1].Color = Color;
	Quad->v[2].Color = Color;
	Quad->v[3].Color = Color;
}

void ValuesSwap(u32 *values, u32 i, u32 j)
{
	u32 tmp = values[i];
	values[i] = values[j];
	values[j] = tmp;
}

void SwapQuadsAndValues(sort_data *Data, u32 i, u32 j)
{
	if(Data->Quads && Data->RandomValues)
	{
		QuadSwap(&Data->Quads->Quads[i], &Data->Quads->Quads[j]);
		TopSwap(&Data->Quads->Top[i], &Data->Quads->Top[j]);
		ValuesSwap(Data->RandomValues->values, i, j);
	}
}

void SortContextSleepNoTimeout(sort_context *Context)
{
	struct timespec ts;
	ts.tv_nsec = Context->delayNs;
	ts.tv_sec = 0;
	clock_nanosleep(CLOCK_MONOTONIC, 0, &ts, 0);
}

void SortContextSleep(sort_context *Context)
{
	Context->genIndex++;
	if(Context->genIndex == Context->SortConfig[Context->currentIteration].genTimeout)
	{
		Context->genIndex = 0;

		struct timespec ts;
		ts.tv_nsec = Context->delayNs;
		ts.tv_sec = 0;
		clock_nanosleep(CLOCK_MONOTONIC, 0, &ts, 0);
	}
}

void SleepSec(unsigned int seconds)
{
	struct timespec ts = {.tv_sec = seconds};
	clock_nanosleep(CLOCK_MONOTONIC, 0, &ts, 0);
}

void SleepNano(u64 nanoseconds)
{
	struct timespec ts =
	{
		.tv_sec = nanoseconds / 1E9,
		.tv_nsec = nanoseconds % (u64)1E9
	};
	clock_nanosleep(CLOCK_MONOTONIC, 0, &ts, 0);
}

void SleepMilli(double milliseconds)
{
	SleepNano(milliseconds * 1E6);
}


/**********************************
* Sorts
**********************************/
#include "sorts.c"


// Draw extra information to further help visualise the sort
void AuxillaryQuadsDraw(sort_context *Context)
{
	if(Context->DrawSwaps)
	{
		glColor3f(0, 0, 0);
		gl_QuadDrawSingle(Context->Data->Quads, Context->AuxSwap.begin);
		gl_QuadDrawSingle(Context->Data->Quads, Context->AuxSwap.end);
	}

	if(Context->DrawShellRange)
	{
		glColor3f(1, 0, 0);
		for(int i = Context->AuxShellRange.begin; i < Context->AuxShellRange.end; i++)
		{
			gl_QuadDrawSingle(Context->Data->Quads, i);
		}
	}

	if(Context->DrawQuickPivot)
	{
		glColor3f(1, 0, 0);
		for(int i = Context->AuxQuickPivot.begin; i < Context->AuxQuickPivot.end; i++)
		{
			gl_QuadDrawSingle(Context->Data->Quads, i);
		}
	}
}

void ContextAuxillaryStateReset(sort_context *Context)
{
	Context->DrawShellRange = 0;
	Context->DrawSwaps = 0;
	Context->DrawQuickPivot = 0;
}


pfnSort GlobalSortFunctionPointers[ST_TOTAL] =
{
	[ST_QUICK] = Quick,
	[ST_SHELL_V1] = Shell,
	[ST_SHELL_V2] = Shell2,
	[ST_QUICK_SHELL] = QuickShell,
	[ST_MERGE_INPLACE] = MergeInPlace,
	[ST_MERGE_BOTTOM_UP] = WikiMergeBottomUp,
	[ST_MERGE_TOP_DOWN] = WikiMergeTopDown,
	[ST_RADIX65536] = Radix65536,
	[ST_RADIX256] = Radix256,
	[ST_RADIX10] = Radix10,
	[ST_RADIX4] = Radix4,
	[ST_RADIX3] = Radix3,
	[ST_RADIX2] = Radix2,
	[ST_BUBBLE] = Bubble,
	[ST_INSERTION] = Insertion,
	[ST_COCKTAIL] = Cocktail,
	[ST_SELECTION] = Selection,
	[ST_BOGO] = Bogo,
	[ST_COUNTING] = Counting,
};

pfnRand GlobalRandomFunctions[RND_FUNC_TOTAL] =
{
	[RND_RANDOM] = RndRandom,
	[RND_ALMOST_SORTED] = RndAlmostSorted,
	[RND_ALMOST_SORTED_AT_BEG] = RndAlmostSortedAtBeg,
	[RND_ALMOST_SORTED_AT_END] = RndAlmostSortedAtEnd,
	[RND_REVERSE] = RndReverse,
};


// @Hack
extern sort_config GlobalSortConfig[];
void SortContextDisplayProperties(sort_context *Context)
{
	int dummy = 0;
	int dim = 12; //  RealGlyphDim();
	f32 ms = (f32)Context->delayNs / 1E6f;
	// u32 timeout = Context->ActiveSort == ST_SHUFFLE ? 0 : Context->SortConfig[Context->currentIteration].genTimeout;

	immf_DrawString(0, 0, dim, dim, 0xFFDDDDDD,
			"\n" /* This line is intentionally left blank (tm) */
			"Elements:        %u\n"
			"Range:           %u - %u\n"
			"Accesses:        %u\n"
			"Comparisons:     %u\n"
			"Delay:           %.4fms\n"
			"Visual time:     %.1fs\n"
			, /* Last item */

			Context->Data->RandomValues->count,

			Context->Data->RandomValues->min + 1,
			Context->Data->RandomValues->max,
			Context->accesses,
			Context->comparisons,
			ms,
			Context->secondsForCurrentRun,
			dummy /* Dummy variable so we don't worry about commas */
			);
	assert(GlobalSortDescriptions[Context->ActiveSort]);
	immf_DrawString(0, 0, dim, dim, 0xFFFFCC33,
			"%s", GlobalSortDescriptions[Context->ActiveSort]);

	if(Context->ActiveSort == ST_SHUFFLE)
	{
		int nextSortIndex = Context->currentIteration;
		const char *nextSortDescription = GlobalSortDescriptions[Context->SortConfig[nextSortIndex].SortType];
		immf_DrawString(90, 0, dim, dim, 0x88FF7700,
			"...%s", nextSortDescription);
	}
}

// Allow for consistent real-time delay durations regardless of element count
u64 MapElementCountToDelay(u64 elements, u64 delayNs)
{
	return (f64)delayNs / ((f64)elements / 1000.0f);
}


void Shuffle(sort_context *Context)
{
	snd_Stop(SND_SOURCE_LEFT); // @Hack
	snd_Play(SND_SOURCE_RIGHT);
	Context->ActiveSort = ST_SHUFFLE;
	pfnRand Randomize = GlobalRandomFunctions[Context->ActiveRandFunc];
	Randomize(Context->Data->RandomValues);
	// RandomValuesRandomize(Context->Data->RandomValues);
	u64 count = Context->Data->RandomValues->count;
	u64 delay = MapElementCountToDelay(count, RANDOMIZE_DELAY_DEFAULT);
	Context->delayNs = delay;
	QuadsMapToRandomValues(Context->Data->Quads, Context->Data->RandomValues, true, delay, &Context->accesses);
	snd_Stop(SND_SOURCE_RIGHT);
}

void VisualClockPause()
{
	pthread_mutex_lock(&SortLock);
	SortIsRunning = 0;
	pthread_mutex_unlock(&SortLock);
}

void VisualClockResume()
{
	pthread_mutex_lock(&SortLock);
	SortIsRunning = 1;
	pthread_mutex_unlock(&SortLock);
	pthread_cond_signal(&ClockProcCond);
}


void SortProcMesageSend(sort_proc_message Message)
{
	pthread_mutex_lock(&SortProcMessageLock);
	assert(GlobalSortProcMessageQueue.sp < SORT_MESSAGE_QUEUE_STACK_SIZE);
	GlobalSortProcMessageQueue.Messages[GlobalSortProcMessageQueue.sp++] = Message;
	pthread_mutex_unlock(&SortProcMessageLock);

	// Wake thread up
	pthread_cond_signal(&SortProcMessageCond);
}


// This thread proc simply waits on messages that translate to actions on the sorting thread,
// rather than coding them directly
// Messages are send via SortProcMesageSend
void *SortProcMessageProc(void *arg)
{
	// Note: We can make this a bit more efficient by introducing a `flush` command
	// This way, we can process all of the messages in bulk rather than one at a time,
	// therefore decreasing the number of mutex operations
	sort_context *Context = arg;
	for(;;)
	{
		pthread_mutex_lock(&SortProcMessageLock);
		while(GlobalSortProcMessageQueue.sp > 0)
		{
			sort_proc_message Message = GlobalSortProcMessageQueue.Messages[--GlobalSortProcMessageQueue.sp];
			switch(Message)
			{
				case SPM_CANCEL:
					// Note: The main thread is responsible for calling pthread_join
					// Looks like we have a bug:
					// `terminate called without an active exception`
					// is ocassionally present if we send the SPM_CANCEL message too frequently.
					// (This crashes the program)
					// Have no idea how to resolve this yet...
					// VisualClockPause is unrelated to this bug
					VisualClockPause();

					pthread_mutex_lock(&SortLock);
					Context->secondsForCurrentRun = 0;
					pthread_cancel(Context->threadHandle);
					pthread_detach(Context->threadHandle);
					pthread_mutex_unlock(&SortLock);

				break;

				case SPM_RESTART:
					pthread_create(&Context->threadHandle, 0, SortProc, Context);
				break;

				case SPM_SORT_PREV:
					// Todo: Clamp?
					if(Context->sortConfigCount > 0)
					{
						if(Context->currentIteration > 0)
						{
							Context->currentIteration -= 1;
						}
						else
						{
							Context->currentIteration = Context->sortConfigCount - 1;
						}
					}
				break;

				case SPM_SORT_NEXT:
					Context->currentIteration = (Context->currentIteration + 1) % Context->sortConfigCount;
				break;
			}
		}
		pthread_cond_wait(&SortProcMessageCond, &SortProcMessageLock);
		pthread_mutex_unlock(&SortProcMessageLock);
	}
}

void MemZero(void *dst, size_t bytes)
{
	memset(dst, 0, bytes);
}

void *VisualClockProc(void *arg)
{
	double accumulator = 0;
	u64 t1 = 0;
	u64 t2 = 0;

	sort_context *Context = arg;
	// Note: This is not an accurate way to keep time, but it will do for our purposes
	for(;;)
	{
		pthread_mutex_lock(&SortLock);
		if(!SortIsRunning)
		{
			pthread_cond_wait(&ClockProcCond, &SortLock);
			t1 = platform_TicksGet();
			t2 = t1;
			accumulator = 0;
			Context->secondsForCurrentRun = 0;
		}
		Context->secondsForCurrentRun = accumulator;
		pthread_mutex_unlock(&SortLock);
		SleepMilli(1);
		t2 = platform_TicksGet();
		accumulator += platform_TicksToSeconds(t2 - t1);
		t1 = t2;
	}
}

void *SortProc(void *arg)
{
	sort_context *Context = arg;
	int ignore;
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &ignore);
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &ignore);
	while(!ThreadTimeToQuit)
	{
		for(int i = Context->currentIteration; i < Context->sortConfigCount && !ThreadTimeToQuit; i++)
		{
			// Blank slate for every run
			ContextReset(Context);
			Context->currentIteration = i;


			// "Reallocating" this data is really cheap since we just advance a pointer
			// All memory that was previously used should still be in the cache (if it's small enough to fit there, at least)
			// as long as we keep the allocation order consistent
			u32 min = GlobalSortConfig[i].min;
			u32 max = GlobalSortConfig[i].max;
			assert(max > min);
			u32 elementCount = max - min;
			u32 previousElementcount = Context->Data->RandomValues->count;
			PermanentClear();
			Context->Data->Quads = QuadsCreate(elementCount);
			Context->Data->RandomValues = RandomValuesCreate(min, max, elementCount);
			assert(Context->Data->RandomValues->count == Context->Data->Quads->count);

			// Clear vertex memory for OpenGL
			// Note: Not thread safe
			if(elementCount != previousElementcount)
			{
				MemZero(Context->Data->Quads->Quads, elementCount * sizeof *Context->Data->Quads->Quads);
				MemZero(Context->Data->Quads->Top, elementCount * sizeof *Context->Data->Quads->Top);
			}


			pthread_mutex_lock(&SortLock);
			TimeToSortAgain = 0;
			SortingHasFinished = 0;
			pthread_mutex_unlock(&SortLock);

			// Sound properties
			snd_GlobalSoundCollection.min = min;
			snd_GlobalSoundCollection.max = max;

			Context->ActiveRandFunc = Context->SortConfig[i].RandFunc;
			Shuffle(Context);
			SleepSec(1);

			sort_type ActiveSort = Context->SortConfig[i].SortType;
			pfnSort SortFunc = GlobalSortFunctionPointers[ActiveSort];
			assert(SortFunc);
			Context->ActiveSort = ActiveSort;
			Context->delayNs = Context->SortConfig[i].delayNs;

			// Uncomment this line to have delay be proportional to element count
			Context->delayNs = MapElementCountToDelay(elementCount,  Context->SortConfig[i].delayNs);

			// Sort!

			snd_Play(SND_SOURCE_RIGHT);

			VisualClockResume();
			Context->accesses = 0;
			SortFunc(Context);

			// Done sorting

			pthread_mutex_lock(&SortLock);
			SortIsRunning = 0;
			pthread_mutex_unlock(&SortLock);

			ContextAuxillaryStateReset(Context);
			QuadsCompletionColorSet(Context);

			snd_Stop(SND_SOURCE_RIGHT);

			QuadsMapToRandomValues(Context->Data->Quads, Context->Data->RandomValues, false, RANDOMIZE_DELAY_DEFAULT, NULL);
			// Set color back to the original

			pthread_mutex_lock(&SortLock);
			// SortingHasFinished = 1;

			// We let this 'auto-play' for now
			// if(!TimeToSortAgain)
			{
				// pthread_cond_wait(&SortAgainCond, &SortAgainLock);
			}
			pthread_mutex_unlock(&SortLock);
			SleepSec(2);

			// By this time, VisualClockProc would have been put on wait and we don't need to lock anything to modify secondsForCurrentRun
			Context->secondsForCurrentRun = 0;
		}
		Context->currentIteration = 0;
	}
	pthread_exit(0);
}

void DebugPrintValues(random_values *Values)
{
	for(int i = 0; i < Values->count; i++)
	{
		printf("%d\n", Values->values[i]);
	}
}

void Sighandler(int signal)
{
	switch(signal)
	{
		case SIGINT:
			glfwTerminate();
			exit(0);
			break;
	}
}

void DrawLoadingScreen(GLFWwindow *Window)
{
	// Loading screen
	glClearColor(BG_COLOR, 1);
	glClear(GL_COLOR_BUFFER_BIT);
	const char text[] = "LOADING...";
	const int letterCount = ArrayCount(text) - 1;

	int dim = 20;
	int x = VIRTUAL_WIDTH / 2 - (dim * letterCount / 2);
	int y = VIRTUAL_HEIGHT / 2 - dim;
	immf_DrawStringBase(x, y, dim, dim, 0xFFFFFFFF, text, letterCount);

	glfwSwapBuffers(Window);
}

// This is our sort "program"
sort_config GlobalSortConfig[] =
{
	{.SortType = ST_INSERTION, 			.RandFunc = RND_RANDOM, 		.delayNs = 5000, 		.genTimeout = 1, 	.min = 0, .max = 1000},
	{.SortType = ST_BUBBLE, 			.RandFunc = RND_RANDOM, 		.delayNs = 5000, 		.genTimeout = 1, 	.min = 0, .max = 1000},
	{.SortType = ST_SELECTION, 			.RandFunc = RND_RANDOM, 		.delayNs = 5000, 		.genTimeout = 1, 	.min = 0, .max = 1000},
	{.SortType = ST_COCKTAIL, 			.RandFunc = RND_RANDOM, 		.delayNs = 5000, 		.genTimeout = 1, 	.min = 0, .max = 1000},
	{.SortType = ST_QUICK, 				.RandFunc = RND_RANDOM, 		.delayNs = 1000000, 	.genTimeout = 1, 	.min = 0, .max = 1000},
	{.SortType = ST_SHELL_V1, 			.RandFunc = RND_RANDOM, 		.delayNs = 1000000, 	.genTimeout = 1, 	.min = 0, .max = 1000},
	{.SortType = ST_SHELL_V2, 			.RandFunc = RND_RANDOM, 		.delayNs = 1000000, 	.genTimeout = 1, 	.min = 0, .max = 1000},
	{.SortType = ST_MERGE_TOP_DOWN, 	.RandFunc = RND_RANDOM, 		.delayNs = 1000000, 	.genTimeout = 1, 	.min = 0, .max = 1000},
	{.SortType = ST_MERGE_BOTTOM_UP,	.RandFunc = RND_RANDOM, 		.delayNs = 1000000, 	.genTimeout = 1, 	.min = 0, .max = 1000},
	{.SortType = ST_MERGE_INPLACE, 		.RandFunc = RND_RANDOM, 		.delayNs = 1000000, 	.genTimeout = 1, 	.min = 0, .max = 1000},
	{.SortType = ST_COUNTING, 			.RandFunc = RND_RANDOM, 		.delayNs = 1000000, 	.genTimeout = 1, 	.min = 0, .max = 1000},
	{.SortType = ST_RADIX2, 			.RandFunc = RND_RANDOM, 		.delayNs = 1000000, 	.genTimeout = 1, 	.min = 0, .max = 1000},
	{.SortType = ST_RADIX3,	 			.RandFunc = RND_RANDOM, 		.delayNs = 1000000, 	.genTimeout = 1, 	.min = 0, .max = 1000},
	{.SortType = ST_RADIX4, 			.RandFunc = RND_RANDOM, 		.delayNs = 1000000, 	.genTimeout = 1, 	.min = 0, .max = 1000},
	{.SortType = ST_RADIX10, 			.RandFunc = RND_RANDOM, 		.delayNs = 1000000, 	.genTimeout = 1, 	.min = 0, .max = 1000},
	{.SortType = ST_RADIX256, 			.RandFunc = RND_RANDOM, 		.delayNs = 1000000, 	.genTimeout = 1, 	.min = 0, .max = 1000},
	{.SortType = ST_RADIX65536, 		.RandFunc = RND_RANDOM, 		.delayNs = 1000000, 	.genTimeout = 1, 	.min = 0, .max = 1000},

	// DISCLAIMER:
	// DON'T ENABLE THIS IF YOU ARE SENSITIVE TO FLASHING LIGHTS
	// {.SortType = ST_BOGO, 				.RandFunc = RND_RANDOM, 		.delayNs = 250000, 		.genTimeout = 1, 	.min = 0, .max = 8},
};


int main(int argc, char **argv)
{
	GLFWwindow *Window;
	const GLFWvidmode *Vidmode;

	glfwInit();
	GlobalMonitor = glfwGetPrimaryMonitor();
	Vidmode = glfwGetVideoMode(GlobalMonitor);
	Window = glfwCreateWindow(Vidmode->width, Vidmode->height, "Sorts", GlobalMonitor, 0);

	GlobalClientWindow.windowedWidth = Vidmode->width / 2;
	GlobalClientWindow.windowedHeight = Vidmode->height / 2;
	GlobalClientWindow.xpos = (Vidmode->width / 2) - (GlobalClientWindow.windowedWidth / 2);
	GlobalClientWindow.ypos = (Vidmode->height / 2) - (GlobalClientWindow.windowedHeight / 2);

	// Callbacks
	glfwSetFramebufferSizeCallback(Window, CbResize);
	glfwSetKeyCallback(Window, CbKeys);

	// Mouse
	glfwSetInputMode(Window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

	glfwMakeContextCurrent(Window);
	glfwSwapInterval(1);

	glOrtho(0, ORTHO_WIDTH, 0, ORTHO_HEIGHT, 0, 1);

	MemoryRegionsInit();


	// Initialize glyph renderer
	immf_WindowDimensions(VIRTUAL_WIDTH, VIRTUAL_HEIGHT);
	immf_VerticalFlip(1);
	immf_FontSpacingVert(2);
	immf_FilterMode(IMM_FILTER_MODE_NONE);
	immf_Init(8, 8);

	// Must only be called after initializing glfw, OpenGL and immf
	DrawLoadingScreen(Window);

	// Sound code
	snd_Init();
	snd_WaveInit();


	// Sort data
	random_values *Values;
	quads *Quads;

	// @Hack
	// We need to create something so that these pointers are valid
	// They get reallocated almost immediately in SortProc,
	// but because of race conditions OpenGL will most likely recieve null pointers if we don't do this
	// See GlobalSortConfig for the actual ranges that these variables map to
	Quads = QuadsCreate(0);
	Values = RandomValuesCreate(0, 0, 1);

	// Sort context
	sort_data SortData = {.Quads = Quads, .RandomValues = Values};
	sort_context SortContext = {.Data = &SortData, .SortConfig = GlobalSortConfig, .sortConfigCount = ArrayCount(GlobalSortConfig)};
	pGlobalSortContext = &SortContext;

	// Sort thread
	signal(SIGINT, Sighandler);
	pthread_create(&SortContext.threadHandle, 0, SortProc, &SortContext);

	// Message thread
	pthread_t messageThread;
	pthread_create(&messageThread, 0, SortProcMessageProc, &SortContext);

	// Clock thread
	pthread_t clockThread;
	pthread_create(&clockThread, 0, VisualClockProc, &SortContext);

	glClearColor(BG_COLOR, 1);

	//
	// Main loop
	//
	while(GlobalRunning)
	{
		glClear(GL_COLOR_BUFFER_BIT);
		glfwPollEvents();

		double t1 = glfwGetTime();

		gl_QuadsDraw(Quads);
		SortContextDisplayProperties(&SortContext);
		AuxillaryQuadsDraw(&SortContext);
		DrawHelpMessage();

		glfwSwapBuffers(Window);

		// Get a new color for the top of the bars
		const float f = 7;
		GlobalBarTopColor.r = cos(f * (t1 + 0));
		GlobalBarTopColor.g = cos(f * (t1 + 2 * PI / 3));
		GlobalBarTopColor.b = cos(f * (t1 + 4 * PI / 3));

		GlobalBarTopColor.r /= 2;
		GlobalBarTopColor.g /= 2;
		GlobalBarTopColor.b /= 2;

		GlobalBarTopColor.r += 0.5;
		GlobalBarTopColor.g += 0.5;
		GlobalBarTopColor.b += 0.5;
	}

	bool finished;
	pthread_mutex_lock(&SortLock);
	ThreadTimeToQuit = 1;
	finished = SortingHasFinished;
	pthread_mutex_unlock(&SortLock);
	if(!finished)
	{
		pthread_kill(SortContext.threadHandle, SIGINT);
	}
	pthread_cond_broadcast(&SortCond);
	pthread_join(SortContext.threadHandle, 0);

	glfwTerminate();
}
