#if 0

// Sample program

#define XS_IMPLEMENTATION
#include "xorshift.h"
#include <stdio.h>
#include <stdint.h>


int main()
{
	xs_state32 State;
	uint32_t random_number;

	// Seed the state with something above 0
	State.state = 42;

	random_number = xs_u32(&State);

	printf("My random number is: %u\n", random_number);
}

#endif

// Allow the user of this library to use a custom function prefix
#ifndef XS_DEF
#ifdef XS_STATIC
#define XS_DEF static
#else
#define XS_DEF extern
#endif /* XS_STATIC */
#endif /* XS_DEF */

#define XS_UINT32_MAX 4294967295U
#define XS_UINT64_MAX 18446744073709551615ULL

#define XS_INT32_MAX 2147483647LL
#define XS_INT32_MIN -2147483648LL

#define XS_INT64_MAX 9223372036854775807LL
#define XS_INT64_MIN -9223372036854775808LL

//--------------------------------------------------------------------------------
// Types
//--------------------------------------------------------------------------------
typedef uint32_t xs__u32;
typedef uint64_t xs__u64;

typedef int32_t xs__s32;
typedef int64_t xs__s64;

typedef float xs__f32;
typedef double xs__f64;


//--------------------------------------------------------------------------------
// Structs
//--------------------------------------------------------------------------------
typedef struct
{
	xs__u32 state;
} xs_state32;

typedef struct
{
	xs__u64 state;
} xs_state64;


//--------------------------------------------------------------------------------
// Declarations and interface
//--------------------------------------------------------------------------------
XS_DEF xs__u32 xs_u32(xs_state32 *State);
XS_DEF xs__u64 xs_u64(xs_state64 *State);
// Base functions. Called by the other routines. Seed them with a state greater than 0.


XS_DEF xs__f32 xs_f32(xs_state32 *State);
XS_DEF xs__f64 xs_f64(xs_state64 *State);
// Normalised value from 0 to 1. Value generated from 0 to XS_UINT32 or XS_UINT64_MAX max, corresponding to the function suffix.


XS_DEF xs__f32 xs_range_in_f32(xs_state32 *State, xs__f32 min, xs__f32 max);
XS_DEF xs__f64 xs_range_in_f64(xs_state64 *State, xs__f64 min, xs__f64 max);
// Normalised value from min to max. Value generated from 0 to XS_UINT32 or XS_UINT64_MAX max, corresponding to the function suffix.
// It is then mapped to the desired range.


XS_DEF xs__u32 xs_range_in_u32(xs_state32 *State, xs__u32 min, xs__u32 max);
XS_DEF xs__u64 xs_range_in_u64(xs_state64 *State, xs__u64 min, xs__u64 max);
// Generates a value from min to max (unsigned inclusive max)


XS_DEF xs__u32 xs_range_ex_u32(xs_state32 *State, xs__u32 min, xs__u32 max);
XS_DEF xs__u64 xs_range_ex_u64(xs_state64 *State, xs__u64 min, xs__u64 max);
// Generates a value from min to max (unsigned exclusive max).


XS_DEF xs__s32 xs_range_in_s32(xs_state32 *State, xs__s32 min, xs__s32 max);
XS_DEF xs__s64 xs_range_in_s64(xs_state64 *State, xs__s64 min, xs__s64 max);
// Generates a value from min to max (signed inclusive max)

XS_DEF xs__s32 xs_range_ex_s32(xs_state32 *State, xs__s32 min, xs__s32 max);
XS_DEF xs__s64 xs_range_ex_s64(xs_state64 *State, xs__s64 min, xs__s64 max);
// Generates a value from min to max (signed exclusive max)


#ifdef XS_IMPLEMENTATION
// Define this to include the function definitions

//--------------------------------------------------------------------------------
// Base xorshift functions, both 32 and 64 bit
// They expect a state with a seed value greater than 0
//--------------------------------------------------------------------------------
XS_DEF xs__u32 xs_u32(xs_state32 *State)
{
	xs__u32 x;
	x = State->state;

	x ^= x << 13;
	x ^= x >> 17;
	x ^= x << 5;

	State->state = x;
	return x;
}

XS_DEF xs__u64 xs_u64(xs_state64 *State)
{
	xs__u64 x;
	x = State->state;

	x ^= x << 13;
	x ^= x >> 7;
	x ^= x << 17;

	State->state = x;
	return x;
}



//--------------------------------------------------------------------------------
// Utility fuctions
//--------------------------------------------------------------------------------

// Normalised (0 - 1)
//--------------------------------------------------------------------------------
XS_DEF xs__f32 xs_f32(xs_state32 *State)
{
	xs__f32 result;
	result = (xs__f32)xs_u32(State) / (xs__f32)XS_UINT32_MAX;
	return result;
}

XS_DEF xs__f64 xs_f64(xs_state64 *State)
{
	xs__f64 result;
	result = (xs__f64)xs_u64(State) / (xs__f64)XS_UINT64_MAX;
	return result;
}

// Range unsigned (inclusive)
//--------------------------------------------------------------------------------
XS_DEF xs__u32 xs_range_in_u32(xs_state32 *State, xs__u32 min, xs__u32 max)
{
	xs__u32 result;
	result = (xs_u32(State) % (max - min + 1)) + min;
	return result;
}

XS_DEF xs__u64 xs_range_in_u64(xs_state64 *State, xs__u64 min, xs__u64 max)
{
	xs__u64 result;
	result = (xs_u64(State) % (max - min + 1)) + min;
	return result;
}

// Range unsigned (exclusive)
//--------------------------------------------------------------------------------
XS_DEF xs__u32 xs_range_ex_u32(xs_state32 *State, xs__u32 min, xs__u32 max)
{
	xs__u32 result;
	result = (xs_u32(State) % (max - min)) + min;
	return result;
}

XS_DEF xs__u64 xs_range_ex_u64(xs_state64 *State, xs__u64 min, xs__u64 max)
{
	xs__u64 result;
	result = (xs_u64(State) % (max - min)) + min;
	return result;
}


// Range signed (inclusive)
//--------------------------------------------------------------------------------
XS_DEF xs__s32 xs_range_in_s32(xs_state32 *State, xs__s32 min, xs__s32 max)
{
	xs__s32 result;
	result = (xs_u32(State) % (max - min + 1)) + min;
	return result;
}

XS_DEF xs__s64 xs_range_in_s64(xs_state64 *State, xs__s64 min, xs__s64 max)
{
	xs__s64 result;
	result = (xs_u64(State) % (max - min + 1)) + min;
	return result;
}

// Range signed (exclusive)
//--------------------------------------------------------------------------------
XS_DEF xs__s32 xs_range_ex_s32(xs_state32 *State, xs__s32 min, xs__s32 max)
{
	xs__s32 result;
	result = (xs_u32(State) % (max - min)) + min;
	return result;
}

XS_DEF xs__s64 xs_range_ex_s64(xs_state64 *State, xs__s64 min, xs__s64 max)
{
	xs__s64 result;
	result = (xs_u64(State) % (max - min)) + min;
	return result;
}

// Normalised (range, floating point)
//--------------------------------------------------------------------------------
XS_DEF xs__f32 xs_range_in_f32(xs_state32 *State, xs__f32 min, xs__f32 max)
{
	xs__f32 result;
	result = xs_f32(State) * (max - min) + min;
	return result;
}

XS_DEF xs__f64 xs_range_in_f64(xs_state64 *State, xs__f64 min, xs__f64 max)
{
	xs__f64 result;
	result = xs_f64(State) * (max - min) + min;
	return result;
}

#endif /* XS_IMPLEMENTATION */
