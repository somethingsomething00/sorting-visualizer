// Bubble
//--------------------------------
void Bubble(sort_context *Context)
{
	int i;
	int swapped;
	int sorted = 0;
	u32 count = Context->Data->RandomValues->count;

	u32 *values = Context->Data->RandomValues->values;

	while(!sorted)
	{
		swapped = 0;
		for(i = 0; i < count - 1; i++)
		{
			if(values[i] > values[i + 1])
			{
				Context->AuxSwap.begin = i;
				Context->AuxSwap.end = i + 1;
				SwapQuadsAndValues(Context->Data, i, i + 1);
				swapped = 1;
				Context->accesses++;
				snd_PitchSet(SND_SOURCE_RIGHT, values[i + 1]);
			}
			SortContextSleep(Context);
			Context->comparisons++;
		}
		snd_PitchSet(SND_SOURCE_LEFT, values[count]);
		count--;
		if(!swapped) sorted = 1;
	}

	snd_Stop(SND_SOURCE_LEFT);
}

// Cocktail
//--------------------------------
void Cocktail(sort_context *Context)
{
	int i;
	int begin;
	int end;
	int swapped;
	int sorted = 0;
	u32 count = Context->Data->RandomValues->count;

	u32 *values = Context->Data->RandomValues->values;

	// Context->DrawSwaps = 1;
	begin = 0;
	end = count - 1;

	while(!sorted)
	{
		swapped = 0;
		for(i = begin; i < end; i++)
		{
			if(values[i] > values[i + 1])
			{
				Context->AuxSwap.begin = i;
				Context->AuxSwap.end = i + 1;
				SwapQuadsAndValues(Context->Data, i, i + 1);
				Context->accesses++;
				swapped = 1;
			}
			snd_PitchSet(SND_SOURCE_RIGHT, values[i + 1]);
			Context->comparisons++;
			SortContextSleep(Context);
		}
		begin++;
		end--;
		for(i = end; i >= begin; i--)
		{
			if(values[i] < values[i - 1])
			{
				Context->AuxSwap.begin = i;
				Context->AuxSwap.end = i - 1;
				SwapQuadsAndValues(Context->Data, i, i - 1);
				Context->accesses++;
				swapped = 1;
			}
			snd_PitchSet(SND_SOURCE_RIGHT, values[i - 1]);
			Context->comparisons++;
			SortContextSleep(Context);
		}
		if(!swapped) sorted = 1;
	}
}


// Insertion
//--------------------------------
void Insertion(sort_context *Context)
{
	int i = 1;
	u32 count = Context->Data->RandomValues->count;
	u32 *values = Context->Data->RandomValues->values;

	// snd_Play(SND_SOURCE_LEFT);

	while(i < count)
	{
		int j = i;
		while(j > 0 && values[j - 1] > values[j])
		{
			SwapQuadsAndValues(Context->Data, j, j - 1);
			snd_PitchSet(SND_SOURCE_RIGHT, values[j]);
			SortContextSleep(Context);

			Context->comparisons++;
			Context->accesses++;

			j--;
		}
		snd_PitchSet(SND_SOURCE_LEFT, values[i]);
		Context->accesses++;
		i++;
	}

	// snd_Stop(SND_SOURCE_LEFT);
}

// Selection
//--------------------------------
void Selection(sort_context *Context)
{
	snd_Play(SND_SOURCE_LEFT);

	u32 count = Context->Data->RandomValues->count;
	u32 *values = Context->Data->RandomValues->values;
	u32 smallest = 0;
	u32 smallestIdx = 0;
	u32 begin = 0;
	int swapped = 0;

	for(;;)
	{
		swapped = 0;
		smallest = values[begin];
		for(u32 i = begin; i < count; i++)
		{
			if(values[i] < smallest)
			{
				smallest = values[i];
				smallestIdx = i;
				swapped = 1;
			}
			Context->comparisons++;
			// snd_PitchSet(SND_SOURCE_RIGHT, values[i]); // Either set the pitch for every comparison, or for every swap
			snd_PitchSet(SND_SOURCE_LEFT, begin);
			SortContextSleep(Context);
		}
		if(swapped != 0)
		{
			snd_PitchSet(SND_SOURCE_RIGHT, values[begin]);
			Context->accesses += 2;
			SwapQuadsAndValues(Context->Data, begin, smallestIdx);
		}
		else
		{
			if(begin == count) break;
		}
		begin++;
	}
	snd_Stop(SND_SOURCE_LEFT);
}


// Quick
// Adapted from the C Programming Language book by Kernighan and Ritchie
//--------------------------------
void QuickPartition(sort_context *Context, int left, int right)
{
	if(left >= right) {Context->comparisons++; return;}

	u32 *arr = Context->Data->RandomValues->values;


	int i;
	int last;

	int part = (left + right) / 2;

	SwapQuadsAndValues(Context->Data, left, part); // Pick `part` element as partition
	snd_PitchSet(SND_SOURCE_RIGHT, arr[left]);

	SortContextSleep(Context);

	Context->accesses++;
	last = left;


	for(i = left + 1; i <= right; i++)
	{
		if(arr[i] < arr[left])
		{
			last++;
			SwapQuadsAndValues(Context->Data, last, i);
			Context->AuxSwap.begin = last;
			Context->AuxSwap.end = i;
			Context->accesses++;
		}
		Context->AuxQuickPivot.begin = last;
		Context->AuxQuickPivot.end = i;
		Context->comparisons++;
		snd_PitchSet(SND_SOURCE_RIGHT, arr[i]);
		SortContextSleep(Context);
	}

	// Restore partition
	SwapQuadsAndValues(Context->Data, left, last);
	snd_PitchSet(SND_SOURCE_RIGHT, arr[left]);
	SortContextSleep(Context);


	Context->accesses++;
	QuickPartition(Context, left, last - 1);
	QuickPartition(Context, last + 1, right);
}

void Quick(sort_context *Context)
{
	// Context->DrawQuickPivot = 1;
	// Context->DrawSwaps = 1;

	QuickPartition(Context, 0, Context->Data->RandomValues->count - 1);
}

// Shell sort
// Adapted from the C Programming Language book by Kernighan and Ritchie
// Note that the performance degrades horribly when the element count is a power of two,
// hence the improved Shell2 version below which uses a precomputed set of gaps
//--------------------------------
void Shell(sort_context *Context)
{
	u32 count = Context->Data->RandomValues->count;
	u32 *values = Context->Data->RandomValues->values;

	// Context->DrawSwaps = 1;
	// Context->DrawShellRange = 1;

	for(s32 gap = count / 2; gap > 0; gap /=2)
	{
		for(s32 i = gap; i < count; i++)
		{

			for(int j = i - gap;
				j >= 0 && values[j] > values[j + gap];
				j -= gap)
			{
				SwapQuadsAndValues(Context->Data, j, j + gap);
				Context->accesses++;
				Context->comparisons++;

				Context->AuxSwap.begin = j;
				Context->AuxSwap.end = j + gap;

				// Context->AuxShellRange.begin = j;
				// Context->AuxShellRange.end = i - gap;
				snd_PitchSet(SND_SOURCE_RIGHT, values[j]);

				SortContextSleep(Context);
			}
		}
	}
}

// Shell sort V2
// Adapted from the C Programming Language book by Kernighan and Ritchie
// Using gap lookup table from Robert Sedgewick's 1996 paper `Analysis of Shellsort and Related Algorithms`
// https://sedgewick.io/wp-content/themes/sedgewick/papers/1996Shellsort.pdf
void Shell2(sort_context *Context)
{
	u32 count = Context->Data->RandomValues->count;
	u32 *values = Context->Data->RandomValues->values;

	// Context->DrawSwaps = 1;


	static const size_t gaps[16] =
	{
		1391376, 463792, 198768, 86961,
		33936,13776, 4592, 1968,
		861, 336,112, 48,
		21, 7, 3, 1
	};

	for (int k = 0; k < 16; k++)
	{
		int gap = gaps[k];
		for(s32 i = gap; i < count; i++)
		{
			for(int j = i - gap;
					j >= 0 && values[j] > values[j + gap];
					j -= gap)
			{
				SwapQuadsAndValues(Context->Data, j, j + gap);
				Context->accesses++;
				Context->comparisons++;

				Context->AuxSwap.begin = j;
				Context->AuxSwap.end = j + gap;

				// Context->AuxShellRange.begin = j;
				// Context->AuxShellRange.end = i - gap;
				snd_PitchSet(SND_SOURCE_RIGHT, values[j]);

				SortContextSleep(Context);
			}
		}
	}
}


// Merge (in place)
//--------------------------------
void Imsort(sort_context *Context, int l, int u);



void Swap(sort_context *Context, int i, int j)
{
	// u32 *xs =
	snd_PitchSet(SND_SOURCE_RIGHT, Context->Data->RandomValues->values[i]);
	snd_PitchSet(SND_SOURCE_RIGHT, Context->Data->RandomValues->values[j]);
	SwapQuadsAndValues(Context->Data, i, j);


	SortContextSleep(Context);
    // u32 tmp = xs[i]; xs[i] = xs[j]; xs[j] = tmp;
}

void Wmerge(sort_context *Context, int i, int m, int j, int n, int w)
{
	u32 *xs = Context->Data->RandomValues->values;
    while (i < m && j < n)
        Swap(Context, w++, xs[i] < xs[j] ? i++ : j++);
    while (i < m)
        Swap(Context, w++, i++);
    while (j < n)
        Swap(Context, w++, j++);
}



/*
 * sort xs[l, u), and put result to working area w.
 * constraint, len(w) == u - l
 */
void Wsort(sort_context *Context, int l, int u, int w) {
    int m;
    if (u - l > 1)
	{
        m = l + (u - l) / 2;
        Imsort(Context, l, m);
        Imsort(Context, m, u);
        Wmerge(Context, l, m, m, u, w);
    }
    else
	{
        while (l < u)
            Swap(Context, l++, w++);
	}
}

void Imsort(sort_context *Context, int l, int u)
{
	u32 *xs = Context->Data->RandomValues->values;

    int m, n, w;
    if (u - l > 1)
	{
        m = l + (u - l) / 2;
        w = l + u - m;
        Wsort(Context, l, m, w); /* the last half contains sorted elements */
        while (w - l > 2)
		{
            n = w;
            w = l + (n - l + 1) / 2;
            Wsort(Context, w, n, l);  /* the first half of the previous working area contains sorted elements */
            Wmerge(Context, l, l + n - w, n, u, w);
        }
        for (n = w; n > l; --n) /*switch to insertion sort*/
		{
            for (m = n; m < u && xs[m] < xs[m-1]; ++m)
			{
                Swap(Context, m, m - 1);
			}
		}
    }
}

// Source:
// https://github.com/liuxinyu95/AlgoXY/blob/algoxy/sorting/merge-sort/src/mergesort.c
void MergeInPlace(sort_context *Context)
{
	Imsort(Context, Context->Data->RandomValues->min, Context->Data->RandomValues->max);
}



// Merge (bottom up, iterative)
//--------------------------------
// Source: https://en.wikipedia.org/wiki/Merge_sort
void WikiMergeArraysBottomUp(u32 *inVal, u32 *outVal, u32 left, u32 right, u32 end,
		quad *inQuads, quad *outQuads,
		quad_basic *inTops, quad_basic *outTops)
{
	int idxLeft = left;
	int idxRight = right;
	int idxOut;
	int idxToSwap;

	for(idxOut = left; idxOut < end; idxOut++)
	{
		if(idxLeft < right && (idxRight >= end || inVal[idxLeft] <= inVal[idxRight]))
		{
			idxToSwap = idxLeft;
			idxLeft++;
		}
		else
		{
			idxToSwap = idxRight;
			idxRight++;
		}
		outVal[idxOut] = inVal[idxToSwap];
		outQuads[idxOut] = inQuads[idxToSwap];
		outTops[idxOut] = inTops[idxToSwap];
	}
}

void WikiMergeBottomUp(sort_context *Context)
{

	u32 count = Context->Data->RandomValues->count;
	u32 *inVal = Context->Data->RandomValues->values;
	u32 *outVal = ScratchAlloc(sizeof *outVal * count);

	quad *inQuads = Context->Data->Quads->Quads;
	quad *outQuads = ScratchAlloc(sizeof *outQuads * count);

	quad_basic *inTops = Context->Data->Quads->Top;
	quad_basic *outTops = ScratchAlloc(sizeof *outTops * count);

	for(int width = 1; width < count; width *= 2)
	{
		for(int i = 0; i < count; i = i + (width * 2))
		{
			int left = i;
			int right = min(i + width, count);
			int end = min(i + (2 * width), count);
			WikiMergeArraysBottomUp(inVal, outVal, left, right, end, inQuads, outQuads, inTops, outTops);
		}

		for(int i = 0; i < count; i++)
		{
			inVal[i] = outVal[i];
			QuadSwap(&inQuads[i], &outQuads[i]);
			TopSwap(&inTops[i], &outTops[i]);

			snd_PitchSet(SND_SOURCE_RIGHT, outVal[i]);

			Context->comparisons += 1;

			SortContextSleep(Context);
		}
	}
	ScratchClear();
}

// Merge (top down, recursive)
//--------------------------------
// Source: https://en.wikipedia.org/wiki/Merge_sort
#if 0
void WikiMergeArraysTopDown(u32 *inVal, u32 *outVal, u32 left, u32 right, u32 end,
		quad *inQuads, quad *outQuads,
		quad_basic *inTops, quad_basic *outTops)
{
	int idxLeft = left;
	int idxRight = right;
	int idxOut;

	for(idxOut = left; idxOut < end; idxOut++)
	{
		if(idxLeft < right && (idxRight >= end || inVal[idxLeft] <= inVal[idxRight]))
		{
			outVal[idxOut] = inVal[idxLeft];
			outQuads[idxOut] = inQuads[idxLeft];
			outTops[idxOut] = inTops[idxLeft];
			idxLeft++;
		}
		else
		{
			outVal[idxOut] = inVal[idxRight];
			outQuads[idxOut] = inQuads[idxRight];
			outTops[idxOut] = inTops[idxRight];
			idxRight++;
		}
	}
}
#endif

void WikiMergeTopDownProc(u32 *inVal, u32 *outVal, u32 left, u32 middle, u32 right,
		quad *inQuads, quad *outQuads,
		quad_basic *inTops, quad_basic *outTops,
		u64 delayNs,
		quad *pInQuadsOriginal,
		quad_basic *pInTopsOriginal,
		sort_context *Context)
{
	u32 idxLeft = left;
	u32 idxRight = middle;
	u32 idxOut;
	u32 idxToSwap;

	for(idxOut = left; idxOut < right; idxOut++)
	{
		if(idxLeft < middle && (idxRight >= right || inVal[idxLeft] <= inVal[idxRight]))
		{
			idxToSwap = idxLeft;
			idxLeft++;
		}
		else
		{
			idxToSwap = idxRight;
			idxRight++;
		}
		outVal[idxOut] = inVal[idxToSwap];

		QuadSwap(&outQuads[idxOut], &inQuads[idxToSwap]);
		pInQuadsOriginal[idxOut] = outQuads[idxOut];

		TopSwap(&outTops[idxOut], &inTops[idxToSwap]);
		pInTopsOriginal[idxOut] = outTops[idxOut];

		snd_PitchSet(SND_SOURCE_RIGHT, inVal[idxToSwap]);

		SortContextSleep(Context);
	}
}

void WikiMergeTopDownSplit(u32 *BVal, u32 *AVal, u32 left, u32 right,
		quad *BQuads, quad *AQuads,
		quad_basic *BTops, quad_basic *ATops, u64 delayNs,
		quad *pInQuadsOriginal,
		quad_basic *pInTopsOriginal,
		sort_context *Context)
{
	if(right - left <= 1) return;

	u32 middle = (left + right) / 2;
	WikiMergeTopDownSplit(AVal, BVal, left, middle, AQuads, BQuads, ATops, BTops, delayNs, pInQuadsOriginal, pInTopsOriginal, Context);
	WikiMergeTopDownSplit(AVal, BVal, middle, right, AQuads, BQuads, ATops, BTops, delayNs, pInQuadsOriginal, pInTopsOriginal, Context);

	// Note the reversal of in and out parameters
	WikiMergeTopDownProc(BVal, AVal, left, middle, right, BQuads, AQuads, BTops, ATops, delayNs, pInQuadsOriginal, pInTopsOriginal, Context);
}

void WikiMergeTopDown(sort_context *Context)
{
	// Explanation:
	// This implementation clobbers the input and output arrays (i.e, swaps pointers)
	// Since OpenGL displays the original quad and tops arrays, we cannot clobber those
	// In this case, we allocate two new copies of each array type and pass those in to merge sort
	// The merged element (whichever pointer it happens to be) is then copied to the original so that it can be displayed
	// The clobbered pointers are never directly used by OpenGL


	u32 count = Context->Data->RandomValues->count;
	u32 *inVal = Context->Data->RandomValues->values;
	u32 *outVal = ScratchAlloc(sizeof *outVal * count);

	quad *originalQuads = Context->Data->Quads->Quads;
	quad *outQuads = ScratchAlloc(sizeof *outQuads * count);
	quad *inQuads = ScratchAlloc(sizeof *inQuads * count);


	quad_basic *originalTops = Context->Data->Quads->Top;
	quad_basic *inTops = ScratchAlloc(sizeof *inTops * count);
	quad_basic *outTops = ScratchAlloc(sizeof *outTops * count);

	// Recursively sort `out` into `in`
	for(int i = 0; i < count; i++)
	{
		outVal[i] = inVal[i];

		outQuads[i] = originalQuads[i];
		inQuads[i] = originalQuads[i];

		inTops[i] = originalTops[i];
		outTops[i] = originalTops[i];
	}

	WikiMergeTopDownSplit(outVal, inVal, 0, count, outQuads, inQuads, outTops, inTops, Context->delayNs, originalQuads, originalTops, Context);

	ScratchClear();
}


// We use this to just sort the numbers array for bogo sort comparisons
void InternalQuickPartition(u32 *arr, int left, int right)
{
	if(left >= right) return;

	int i;
	int last;

	int part = (left + right) / 2;

	ValuesSwap(arr, left, part);
	last = left;


	for(i = left + 1; i <= right; i++)
	{
		if(arr[i] < arr[left])
		{
			last++;
			ValuesSwap(arr, last, i);
		}
	}

	// Restore partition
	ValuesSwap(arr, left, last);
	InternalQuickPartition(arr, left, last - 1);
	InternalQuickPartition(arr, last + 1, right);
}

void InternalQuickSort(u32 *arr, u32 count)
{
	InternalQuickPartition(arr, 0, count - 1);
}


// B0gO
//--------------------------------
void Bogo(sort_context *Context)
{
	u32 count = Context->Data->RandomValues->count;
	u32 *randomValues = Context->Data->RandomValues->values;

	for(;;)
	{
		int i;
		for(i = 1; i < count; i++)
		{
			Context->comparisons++;
			if(!(randomValues[i - 1] < randomValues[i])) break;
		}
		if(i >= count) break;

		quads *Quads = Context->Data->Quads;
		random_values *Values = Context->Data->RandomValues;
		RandomValuesRandomize(Values);
		QuadsMapToRandomValues(Quads, Values, false, 0, NULL);
		Context->accesses += count;
		SortContextSleep(Context);
	}
}

// Quick / Shell hybrid
//--------------------------------
void QuickShell(sort_context *Context)
{
	u32 count = Context->Data->RandomValues->count;
	u32 *values = Context->Data->RandomValues->values;

	Context->DrawSwaps = 1;
	u32 gapLimit = count / (2 * 4);

	for(s32 gap = count / 2; gap > gapLimit; gap /=2)
	{
		for(s32 i = gap; i < count; i++)
		{

			for(int j = i - gap;
				j >= 0 && values[j] > values[j + gap];
				j -= gap)
			{
				SwapQuadsAndValues(Context->Data, j, j + gap);
				Context->accesses++;

				Context->AuxSwap.begin = j;
				Context->AuxSwap.end = j + gap;
				SortContextSleep(Context);
			}
		}
	}

	Quick(Context);
}

// Radix256
//--------------------------------
void Count256(u32 counter[256], const u32 base, const u32 bitmask, u32 shift, sort_data *Data, u32 *auxValues, quad *auxQuads, quad_basic *auxTops, u32 len);
void Radix256(sort_context *Context)
{
	u32 counter[256];
	int iterations;
	quad *auxQuads;
	quad_basic *auxTops;
	u32 *auxValues;
	u32 shift = 0;

	// We only support 4 byte integers right now
	assert(sizeof *Context->Data->RandomValues->values == 4);
	const u32 len = Context->Data->RandomValues->count;

	auxValues = ScratchAlloc(sizeof *auxValues * len);
	auxQuads = ScratchAlloc(sizeof *auxQuads * len);
	auxTops = ScratchAlloc(sizeof *auxTops * len);

	u32 *inVal = Context->Data->RandomValues->values;
	quad *inQuads = Context->Data->Quads->Quads;
	quad_basic *inTops = Context->Data->Quads->Top;

	u32 maxVal = Context->Data->RandomValues->max;
	if(0) {}
	else if(maxVal < (1 << 8))
		iterations = 1;
	else if(maxVal < (1 << 16))
		iterations = 2;
	else if(maxVal < (1 << 24))
		iterations = 3;
	else
		iterations = 4;

	for(int i = 0; i < iterations; i++)
	{
		Count256(counter, 256, 256 - 1, shift, Context->Data, auxValues, auxQuads, auxTops, len);
		shift += 8;


		// Copy aux* arrays back to the input
		for(int i = 0; i < len; i++)
		{
			inVal[i] = auxValues[i];
			QuadSwap(&inQuads[i], &auxQuads[i]);
			TopSwap(&inTops[i], &auxTops[i]);

			snd_PitchSet(SND_SOURCE_RIGHT, auxValues[i]);
			Context->accesses += 2;
			// In Count256, we run through the entire array twice

			SortContextSleep(Context);
		}
	}

	ScratchClear();
}

void Count256(u32 counter[256], const u32 base, const u32 bitmask, u32 shift, sort_data *Data, u32 *auxValues, quad *auxQuads, quad_basic *auxTops, u32 len)
{
	u32 *inVal = Data->RandomValues->values;
	quad *inQuads = Data->Quads->Quads;
	quad_basic *inTops = Data->Quads->Top;

	memset(counter, 0, sizeof *counter * base);

	for(int i = 0; i < len; i++)
	{
		u32 rem = (inVal[i] >> shift) & bitmask;
		counter[rem] += 1;
	}

	for(int i = 1; i < base; i++)
	{
		counter[i] += counter[i - 1];
	}

	for(int i = len - 1; i >= 0; i--)
	{
		u32 countIndex = (inVal[i] >> shift) & bitmask;
		counter[countIndex] -= 1;
		u32 outIndex = counter[countIndex];

		auxValues[outIndex] = inVal[i];
		auxQuads[outIndex] = inQuads[i];
		auxTops[outIndex] = inTops[i];
	}
}


// RadixAny
//--------------------------------
void CountAny(u32 *counter, const u32 base, u32 divisor, sort_data *Data, u32 *auxValues, quad *auxQuads, quad_basic *auxTops, u32 len);
void RadixAny(sort_context *Context, u32 base)
{
	quad *auxQuads;
	quad_basic *auxTops;
	u32 *auxValues;

	// @Hardcoded: We only support 4 byte integers right now
	assert(sizeof *Context->Data->RandomValues->values == 4);
	const u32 len = Context->Data->RandomValues->count;

	auxValues = ScratchAlloc(sizeof *auxValues * len);
	auxQuads = ScratchAlloc(sizeof *auxQuads * len);
	auxTops = ScratchAlloc(sizeof *auxTops * len);

	u32 *inVal = Context->Data->RandomValues->values;
	quad *inQuads = Context->Data->Quads->Quads;
	quad_basic *inTops = Context->Data->Quads->Top;

	u32 maxVal = Context->Data->RandomValues->max;
	int iterations = 0;
	do
	{
		iterations++;
		maxVal /= base;
	} while(maxVal);

	int divisor = 1;
	u32 *counter = ScratchAlloc(sizeof *counter * base);
	for(int i = 0; i < iterations; i++)
	{
		CountAny(counter, base, divisor, Context->Data, auxValues, auxQuads, auxTops, len);
		divisor *= base;


		// Copy aux* arrays back to the input
		for(int i = 0; i < len; i++)
		{
			snd_PitchSet(SND_SOURCE_RIGHT, inVal[i]);
			inVal[i] = auxValues[i];
			QuadSwap(&inQuads[i], &auxQuads[i]);
			TopSwap(&inTops[i], &auxTops[i]);

			Context->accesses += 2;
			// In CountAny, we run through the entire array twice
			// snd_PitchSet(SND_SOURCE_RIGHT, auxValues[i]);

			SortContextSleep(Context);
		}
	}
	ScratchClear();
}

// We can generalize any radix base like this when we don't care about getting fancy with bitshifts
#define RADIX(base) \
void Radix##base(sort_context *Context) \
{\
	RadixAny(Context, (base));\
}

RADIX(16)
RADIX(10)
RADIX(4)
RADIX(2)
RADIX(3)
RADIX(65536)

void CountAny(u32 *counter, const u32 base, u32 divisor, sort_data *Data, u32 *auxValues, quad *auxQuads, quad_basic *auxTops, u32 len)
{
	u32 *inVal = Data->RandomValues->values;
	quad *inQuads = Data->Quads->Quads;
	quad_basic *inTops = Data->Quads->Top;

	memset(counter, 0, sizeof *counter * base);

	for(int i = 0; i < len; i++)
	{
		u32 rem = (inVal[i] / divisor) % base;
		counter[rem] += 1;
	}

	for(int i = 1; i < base; i++)
	{
		counter[i] += counter[i - 1];
	}

	for(int i = len - 1; i >= 0; i--)
	{
		u32 countIndex = (inVal[i] / divisor) % base;
		counter[countIndex] -= 1;
		u32 outIndex = counter[countIndex];

		auxValues[outIndex] = inVal[i];
		auxQuads[outIndex] = inQuads[i];
		auxTops[outIndex] = inTops[i];
	}
}


void Counting(sort_context *Context)
{
	u32 *values = Context->Data->RandomValues->values;
	u32 count = Context->Data->RandomValues->count;

	size_t counterLen = Context->Data->RandomValues->max + 1;
	u32 *counter = ScratchAlloc(sizeof *counter * counterLen);
	MemZero(counter, sizeof *counter * counterLen);

	Context->DrawSwaps = true;

	for(int i = 0; i < count; i++)
	{
		counter[values[i]]++;

		snd_PitchSet(SND_SOURCE_RIGHT, values[i]);

		Context->AuxSwap.begin = i;
		Context->AuxSwap.end = i;

		Context->accesses++;

		SortContextSleep(Context);
	}

	u32 valIdx = 0;
	for(int i = 0; i < counterLen; i++)
	{
		for(int j = 0; j < counter[i]; j++)
		{
			values[valIdx] = i;
			valIdx++;
		}

	}

	QuadsMapToRandomValues(Context->Data->Quads, Context->Data->RandomValues, true, Context->delayNs, &Context->accesses);
	ScratchClear();
}
