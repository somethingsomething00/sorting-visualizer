#ifndef _IMMF_H_
#define _IMMF_H_


// An immediate-mode OpenGL bitmap font renderer using the legacy API
// Example usage in code

#if 0
	// Allocate texture memory in OpenGL for each glyph, where each texture is 8x8 pixels
	immf_Init(16 ,16);

	// Dimensions of screen space
	immf_WindowDimensions(800, 600);

	// y=0 is now the top of the screen, as opposed to OpenGL's approach
	immf_VerticalFlip(1);

	// Draw a string at x=20, y=20, where each quad is 16x16 pixels and colour is 0xFFFFFFFF (white)
	// Colour is RGBA little-endian. Alpha blending is supported
	immf_DrawString(20, 20, 16, 16, 0xFFFFFFFF, "Hello, world!");

	// In red
	immf_DrawString(20, 40, 16, 16, 0xFF0000FF, "Hello, world!");

	// In white, with 0xAA for alpha
	immf_DrawString(20, 60, 16, 16, 0xAA0000FF, "Hello, world!");
#endif

#define GLYPH_BPP 4
#define GLYPHS_MAX 512
#define DRAW_STRING_BUF_MAX 2048
#define ATTRIB_STACK_SIZE 64

#define GLYPH_BEGIN 0x20
#define GLYPH_END 0x7F

/**********************************
* Types
**********************************/
typedef struct
{
	u32 *pixels;
	u32 sizeInBytes;
	u32 w;
	u32 h;

	GLuint gl_TexId;
} immf_glyph;

typedef struct
{
	immf_glyph Glyph[GLYPHS_MAX];
} immf_glyphs;

typedef enum
{
	IMM_FILTER_MODE_DUMMY,
	IMM_FILTER_MODE_NONE,
	IMM_FILTER_MODE_BILINEAR,
} immf_filter_mode;

typedef enum
{
	IMM_ATTRIB_SPACING_VERT,
	IMM_ATTRIB_WINDOW_DIMENSIONS,
} immf_attrib_type;

typedef struct
{
	immf_attrib_type Type;
	union
	{
		float spacing;

		struct
		{
			int winWidth;
			int winHeight;
		};
	};
} immf_attrib;

typedef struct
{
	immf_attrib AttribStack[ATTRIB_STACK_SIZE];
	int sp;
} immf_attrib_stack;

typedef struct
{
	float hspacing;
	float vspacing;
	u32 WindowWidth;
	u32 WindowHeight;
	int wordWrap;
	float MatrixWidth;
	float MatrixHeight;
	int VerticalFlip;
	immf_filter_mode FilterMode;

	int Initialised;
} immf_context;

/**********************************
* Globals
**********************************/
immf_glyphs GlobalGlyphs = {0};
immf_context GlobalImmFontContext = {0};
immf_attrib_stack GlobalImmAttribStack = {0};

/**********************************
* Functions
**********************************/
void immf_PushAttrib(immf_attrib_type Type)
{
	if(GlobalImmAttribStack.sp < ATTRIB_STACK_SIZE)
	{
		int sp = GlobalImmAttribStack.sp;
		immf_attrib *Attrib = &GlobalImmAttribStack.AttribStack[sp];
		GlobalImmAttribStack.sp++;
		switch(Type)
		{
			case IMM_ATTRIB_SPACING_VERT:
				Attrib->spacing = GlobalImmFontContext.vspacing;
				break;
			case IMM_ATTRIB_WINDOW_DIMENSIONS:
				Attrib->winWidth = GlobalImmFontContext.WindowWidth;
				Attrib->winHeight = GlobalImmFontContext.WindowHeight;
				break;
			default:
				break;
		}
	}
}

void immf_PopAttrib()
{
	for(int i = 0; i < GlobalImmAttribStack.sp; i++)
	{
		immf_attrib *Attrib = &GlobalImmAttribStack.AttribStack[i];
		switch(Attrib->Type)
		{
			case IMM_ATTRIB_SPACING_VERT:
				GlobalImmFontContext.vspacing = Attrib->spacing;
				break;
			case IMM_ATTRIB_WINDOW_DIMENSIONS:
				GlobalImmFontContext.WindowWidth = Attrib->winWidth;
				GlobalImmFontContext.WindowHeight = Attrib->winHeight;
				break;
			default:
				break;
		}
	}
}

void immf_FontSpacingHorz(float value)
{
	GlobalImmFontContext.hspacing = value;
}

void immf_FontSpacingVert(float value)
{
	GlobalImmFontContext.vspacing = value;
}

void immf_WindowDimensions(u32 width, u32 height)
{
	GlobalImmFontContext.WindowWidth = width;
	GlobalImmFontContext.WindowHeight = width;
	GlobalImmFontContext.MatrixWidth = 2.0f / (float)width;
	GlobalImmFontContext.MatrixHeight = 2.0f / (float)height;
}

int immf_ShouldWrap(u32 x)
{
	int result = 0;

	if(GlobalImmFontContext.wordWrap)
	{
		u32 w = GlobalImmFontContext.WindowWidth;
		if(w && x > w) result = 1;
	}

	return result;
}

void immf_WordWrapEnable()
{
	GlobalImmFontContext.wordWrap = 1;
}

void immf_WordWrapDisable()
{
	GlobalImmFontContext.wordWrap = 0;
}

void immf_VerticalFlip(int flip)
{
	if(flip) GlobalImmFontContext.VerticalFlip = 1;
	else GlobalImmFontContext.VerticalFlip = 0;
}

void immf_FilterMode(immf_filter_mode Mode)
{
	immf_filter_mode Result;
	immf_filter_mode Last;

	Last = GlobalImmFontContext.FilterMode;
	if(Mode < IMM_FILTER_MODE_NONE) Result = IMM_FILTER_MODE_NONE;
	else Result = Mode;

	if(Result == Last) return;

	if(GlobalImmFontContext.Initialised)
	{
		int gl_FilterMode = Result == IMM_FILTER_MODE_NONE ? GL_NEAREST : GL_LINEAR;
		for(int i = 0; i < GLYPHS_MAX; i++)
		{
			immf_glyph *Glyph = &GlobalGlyphs.Glyph[i];
			if(Glyph->gl_TexId)
			{
				glBindTexture(GL_TEXTURE_2D, Glyph->gl_TexId);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, gl_FilterMode);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, gl_FilterMode);
			}
		}
	}

	GlobalImmFontContext.FilterMode = Result;
}
// Note: Specific to this sorting visualizer, we will allocate from a shared scratch memory pool
// As a standalone library, we go with calloc
void *ScratchAlloc(size_t bytes);
void ScratchClear(void);

void immf_gl_GlyphGen(immf_glyph *Glyph)
{
	int FilterMode = (GlobalImmFontContext.FilterMode == IMM_FILTER_MODE_NONE) ? GL_NEAREST : GL_LINEAR;
	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &Glyph->gl_TexId);
	glBindTexture(GL_TEXTURE_2D, Glyph->gl_TexId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, FilterMode);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, FilterMode);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, Glyph->w, Glyph->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, Glyph->pixels);
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
}

void immf_Init(u32 GlyphWidth, u32 GlyphHeight)
{
	// We are using an 8 by 8 font
	assert(GlyphWidth % 8 == 0);
	assert(GlyphHeight % 8 == 0);

	int i;
	u32 whiteness = 0xFF;
	u32 blackness = 0x00;
	const u32 bytesPerLogicalPixel = (GlyphWidth * GLYPH_BPP) / 8;

	// Destination for texture
	// We will support RGB and RGBA for now

	for(i = GLYPH_BEGIN; i < GLYPH_END; i++)
	{
		immf_glyph *Glyph = &GlobalGlyphs.Glyph[i];
		Glyph->w = GlyphWidth;
		Glyph->h = GlyphHeight;

		size_t toAlloc = GlyphWidth * GlyphHeight * sizeof *Glyph->pixels;

		// Replace with calloc in regular code
		Glyph->pixels = ScratchAlloc(toAlloc);

		assert(Glyph->pixels);
		for(size_t i = 0; i < toAlloc / sizeof *Glyph->pixels; i++)
		{
			Glyph->pixels[i] = 0;
		}

		u8 *drow = (u8 *)Glyph->pixels;

		u32 pitch = GlyphWidth * GLYPH_BPP * (GlyphWidth / 8);
		// To the next row in the bitmap

		// Read from bitmap
		// Each set bit in the bitmap corresponds to a region of the texture

		// For each row
		for(int bitmapIndex = 0; bitmapIndex < 8; bitmapIndex++)
		{
			// For each col
			for(int bit = 0; bit < 8; bit++)
			{
				char pixel = (font8x8_basic[i][bitmapIndex] >> bit) & 0x1;
				char pixelColor;
				if(pixel)
				{
					pixelColor = whiteness;
					// putchar('#');
				}
				else
				{
					pixelColor = blackness;
					// putchar(' ');
				}

				u8 *start = &drow[bytesPerLogicalPixel * bit];
				// Start of the texture memory corresponding to the logical pixel

				for(int dy = 0; dy < GlyphWidth / 8; dy++)
				{
					for(int dx = 0; dx < bytesPerLogicalPixel; dx++)
					{
						start[dx] = pixelColor;
					}
					start += GlyphWidth * GLYPH_BPP;
				}
			}
			drow += pitch;
			// putchar('\n');
		}

		// We just generated data for a letter
		// Let's create a GL texture for it
		immf_gl_GlyphGen(Glyph);

		// Replace with free() in regular code
		ScratchClear();
	}

	GlobalImmFontContext.Initialised = 1;
}

void immf_gl_DrawGlyph(float x, float y, float w, float h, u32 texid)
{
	glBindTexture(GL_TEXTURE_2D, texid);
	glBegin(GL_QUADS);

	glTexCoord2f(0, 0);
	glVertex2f(x, y);

	glTexCoord2f(0, 1);
	glVertex2f(x, y + h);

	glTexCoord2f(1, 1);
	glVertex2f(x + w, y + h);

	glTexCoord2f(1, 0);
	glVertex2f(x + w, y);

	glEnd();
}

void immf_DrawStringBase(float x, float y, float w, float h, u32 color, const char *text, const u32 len)
{
	float dx = 0;
	float dy = 0;
	const char *pBuf = text;

	// This is so we can render fonts regardless of how glOrtho is set up
	// Requires calling immf_WindowDimensions to set the desired render dimensions
	float a = GlobalImmFontContext.MatrixWidth;
	float f = GlobalImmFontContext.MatrixHeight;

	if(GlobalImmFontContext.VerticalFlip) f = -f;

	float m[16] =
	{
		a, 0, 0, 0,
		0, f, 0, 0,
		0, 0, 1, 0,
		-1, 1, 1, 1,
	};

	glPushMatrix();
	glLoadMatrixf(m);

	// Note: PushAttrib slows things down quite a bit
	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Color is RGBA (R is LSB)
	glColor4ubv((u8 *)&color);

	// Glyphs
	char c;
	while((c = *pBuf++))
	{
		if(c == '\n')
		{
			dy += h + GlobalImmFontContext.vspacing;
			dx = 0;
			continue;
		}

		if(immf_ShouldWrap(x + dx + w + GlobalImmFontContext.hspacing))
		{
			dy += h + GlobalImmFontContext.vspacing;
			dx = 0;
		}

		immf_glyph *Glyph = &GlobalGlyphs.Glyph[c];
		immf_gl_DrawGlyph(dx + x, dy + y, w, h, Glyph->gl_TexId);

		dx += w + GlobalImmFontContext.hspacing;
	}

	glPopAttrib();
	glPopMatrix();

}


void immf_DrawString(float x, float y, float w, float h, u32 color, const char *fmt, ...)
{
	static char buf[DRAW_STRING_BUF_MAX];
	va_list vl;
	va_start(vl, fmt);
	int bytesWritten = vsnprintf(buf, DRAW_STRING_BUF_MAX - 1, fmt, vl);
	va_end(vl);

	if(bytesWritten < 1) return;

	buf[bytesWritten] = 0;
	immf_DrawStringBase(x, y, w, h, color, buf, bytesWritten);
}

#endif /* _IMMF_H_ */
