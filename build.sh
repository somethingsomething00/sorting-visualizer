#!/bin/sh
cc="gcc"
src="main.c"
bin="sorting-visualizer"
cflags="-std=gnu11"
libs="-lGL -lglfw -lm -pthread -lopenal"
opt="-O2"

compile_command="$cc $src -o $bin $libs $cflags $opt"

echo "$compile_command"
$compile_command
