# Sorting Visualizer
A sorting visualizer written in C, inspired by [W0rthy's](https://www.youtube.com/watch?v=QmOtL6pPcI0) own visualizer.

Features **17** different sorts, with the ability to add your own. Complete with bleep bloops!

![thumbnail](./thumbnail.png)

## Quick Start

### Building
#### Linux
```console
./build.sh
./sorting-visualizer
```
#### Others
**Incomplete**

The code should be fairly portable, with the exception of `pthreads` and `clock_gettime`.
On Windows, you can compile with `mingw` as it has a pthreads wrapper.


## Controls

| Action            | Key(s) |
|-------------------|--------|
| Next Sort         | RIGHT  |
| Previous Sort     | LEFT   |
| Repeat Sort       | UP     |
| (Un)mute Audio    | M      |
| Toggle Fullscreen | F      |
| Toggle Help       | H, F1  |
| Quit              | Q, ESC |


## Dependencies
- C compiler (gcc is used by default, clang works too)
- pthreads
- OpenGL 1.1
- GLFW3
- OpenAL (optional)


## Compile time parameters
See [config.h](./config.h).

## Customizing
Each sort can be configured with its own randomization function, delay, element count, and element range.

See `GlobalSortConfig` in [main.c](./main.c).

### Adding you own sorts
See [sorts.c](./sorts.c) for examples. 

These variables will be of interest to you:
- `GlobalSortFunctionPointers`
- `GlobalSortDescriptions`
- `GlobalSortConfig`

Once you write your sort and configure the variables, to add your sort to the program loop, insert it at the desired position in the `GlobalSortConfig` array. The program works by conceptually treating each sort like a 'track' in a playlist.


## Known bugs
- When advancing or restarting a sort, we terminate the sort thread and start a new one.
Occasionally, doing this restart crashes the program with `terminate called without an active exception`.
I have not been able to track down the root cause of this.
It occurs roughly 1/100 times.
	- See `SPM_CANCEL` in [main.c](./main.c).

- The `accesses` and `comparisons` fields in the HUD are not always meaningful.

## Todo

- The audio work is rather basic. It could definitely be improved. At the moment it's very 90s PC speaker style :)

- There's no ability to pause.

## Final Notes
This visualizer also contains a [bitmap font rendering](./immf_standalone.h) library that I wrote for this program.

Feel free to use it in your own work.

The `visual time` is not necessarily indicative of the performance of the current sorting algorithm. It can give you rough estimate, but its real world performance may be quite different. Do your own tests with real data!


## References
[13 Sorts](https://www.youtube.com/watch?v=QmOtL6pPcI0)

[Sound of Sorting](https://www.youtube.com/watch?v=kPRA0W1kECg)

[Radix Sort (Creel)](https://www.youtube.com/watch?v=_KhZ7F-jOlI)

[JavidX9 - Sound Synthisizer (4 part series)](https://www.youtube.com/watch?v=tgamhuQnOkM)

[GLFW3 Documentation](https://www.glfw.org/docs/latest/)

[OpenGL Documentation](https://docs.gl/)
