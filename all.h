#ifndef _ALL_H_
#define _ALL_H_

#include "config.h"

// Standard
//--------------------------------------------------------------------------------
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <stdlib.h>
#include <math.h>
#include <signal.h>
#include <pthread.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>


// GL
//--------------------------------------------------------------------------------
#include <GL/gl.h>
#include <GLFW/glfw3.h>

#if AUDIO_ENABLED

// OpenAL
//--------------------------------------------------------------------------------
#define AL_ALEXT_PROTOTYPES
#include <AL/al.h>
#include <AL/alc.h>
#include <AL/efx.h>

#endif /* AUDIO_ENABLED */


// Local
//--------------------------------------------------------------------------------
#include "types.h"
#include "font8x8_basic.h"
#include "immf.h"

#define XS_DEF static inline
#define XS_IMPLEMENTATION
#include "xorshift.h"

#endif /* _ALL_H_ */
