u64 platform_TicksGet()
{
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);

	return ts.tv_sec * (u64)1E9 + ts.tv_nsec;
}

double platform_TicksToSeconds(u64 ticks)
{
	return (double)ticks / 1E9;
}
