// The OpenAL sound interface

typedef enum
{
	SND_SOURCE_LEFT,
	SND_SOURCE_RIGHT,

	SND_SOURCE_HELP,

	SND_SOURCE_TOTAL
} snd_source;

typedef struct
{
	ALCcontext *Context;
	ALCdevice *Device;
} snd_context;


typedef struct
{
	ALuint source[SND_SOURCE_TOTAL];
	int min;
	int max;
} snd_sound_collection;

// Globals
snd_context GlobalSoundContext;
snd_sound_collection snd_GlobalSoundCollection;

// for(it_index = 0, it = snd_GlobalSoundCollection.source[it_index]; it_index < SND_SOURCE_TOTAL; it = snd_GlobalSoundCollection.source[++it_index])
// GCC complained about undefined behaviour regarding the above line when optimizations were turned on, so we use this trick in the while loop instead
// `-Wunsafe-loop-optimizations` <-- this warning
#define SND_FOREACH_SOUND(it) \
	int it_index = 0;\
	int it = snd_GlobalSoundCollection.source[it_index];\
	while(it_index < SND_SOURCE_TOTAL && (it = snd_GlobalSoundCollection.source[it_index++]) * 0 == 0)



void snd_CheckAccess(snd_source idx)
{
	assert(idx >= 0 && idx < SND_SOURCE_TOTAL);
}

void snd_Play(snd_source idx)
{
	snd_CheckAccess(idx);
	alSourcePlay(snd_GlobalSoundCollection.source[idx]);
}

void snd_Stop(snd_source idx)
{
	snd_CheckAccess(idx);
	alSourceStop(snd_GlobalSoundCollection.source[idx]);
}

void snd_Pitch(snd_source idx, float pitch)
{
	snd_CheckAccess(idx);
	alSourcef(snd_GlobalSoundCollection.source[idx], AL_PITCH, pitch);
}

void snd_PitchSet(snd_source idx, float i)
{
	float pitch = i / snd_GlobalSoundCollection.max * 0.75;
	pitch += 0.25;
	snd_Pitch(idx, pitch);
}

void snd_MuteAll()
{
	SND_FOREACH_SOUND(sound)
	{
		alSourcei(sound, AL_GAIN, 0);
	}
}

void snd_UnmuteAll()
{
	SND_FOREACH_SOUND(sound)
	{
		alSourcei(sound, AL_GAIN, 1);
	}
}

void snd_Init()
{
	ALCcontext *Context;
	ALCdevice *Device;
	Device = alcOpenDevice(NULL);
	assert(Device != NULL);
	Context = alcCreateContext(Device, NULL);
	assert(Context != NULL);
	alcMakeContextCurrent(Context);

	alListener3f(AL_POSITION, 0, 0, 0);
}

int snd_GenSourceFromBuffer(ALuint buffer, bool looping)
{
	ALuint source;
	alGenSources(1, &source);

	alSourcei(source, AL_BUFFER, buffer);
	alSourcei(source, AL_LOOPING, looping ? AL_TRUE : AL_FALSE);

	return source;
}

#undef PI
#define PI 3.141592654

void snd_WaveInit()
{

	const float frequency = 440;
	const float sampleRate = 48000;
	const float volume = 4000;
	const float seconds = 0.5;
	const float TAU = (PI * 2);
	const u32 waveLen = sampleRate * seconds;


	short *wave = ScratchAlloc(waveLen * sizeof *wave);
	// Generate a sine wave and load buffer

	for(u32 i = 0; i < waveLen; i++)
	{
		float v1 = 0;
		float v2 = 0;
		float v3 = 0;
		float result;

		// Sine wave
		v1 = sin(((TAU * (f32)i) * frequency) / sampleRate);
		v2 = sin(((TAU * (f32)i) * frequency * 1.0) / sampleRate);

		// Square wave
		if(v2 > 0) v2 = 0.2;
		else v2 = -0.2;

		// Sawtooth
		v3 = (2.0 / PI) * ((frequency * 2) * PI * fmod((f32)i / sampleRate, 1.0 / (frequency * 2)) - (PI / 2.0));

		result = v2;
		// result = v2 + v1;
		// result = v3;
		// result = v1 + v2 + v3;

		wave[i] = volume * result;
	}


	ALuint buffer;
	alGenBuffers(1, &buffer);
	alBufferData(buffer, AL_FORMAT_MONO16, wave, waveLen * sizeof *wave, sampleRate);

	snd_GlobalSoundCollection.source[SND_SOURCE_LEFT] = snd_GenSourceFromBuffer(buffer, true);
	snd_GlobalSoundCollection.source[SND_SOURCE_RIGHT] = snd_GenSourceFromBuffer(buffer, true);

	ALuint helpsound;
	alGenBuffers(1, &helpsound);
	alBufferData(helpsound, AL_FORMAT_MONO16, wave, waveLen * sizeof *wave / (8 * 4), sampleRate);
	snd_GlobalSoundCollection.source[SND_SOURCE_HELP] = snd_GenSourceFromBuffer(helpsound, false);
	snd_Pitch(SND_SOURCE_HELP, 0.25);

	ALuint effect;
	ALuint slot;

	alGenEffects(1, &effect);
	alGenAuxiliaryEffectSlots(1, &slot);
	alEffecti(effect, AL_EFFECT_TYPE, AL_EFFECT_REVERB);
	alEffectf(effect, AL_REVERB_DENSITY, 2.0);
	alEffectf(effect, AL_REVERB_DECAY_TIME, 1000.0);
	alGenAuxiliaryEffectSlots(1, &slot);
	alAuxiliaryEffectSloti(slot, AL_EFFECTSLOT_EFFECT, effect);
	alSource3i(snd_GlobalSoundCollection.source[SND_SOURCE_RIGHT], AL_AUXILIARY_SEND_FILTER, slot, 0, 0);

	ScratchClear();
}
