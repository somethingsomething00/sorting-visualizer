#if AUDIO_ENABLED

#include "snd_openal.c"

#else

// Dummy struct
typedef struct
{
	int min;
	int max;
} snd_sound_collection;

snd_sound_collection snd_GlobalSoundCollection = {0};

#define snd_Init(...)
#define snd_WaveInit(...)
#define snd_Play(...)
#define snd_Stop(...)
#define snd_MuteAll(...)
#define snd_UnmuteAll(...)
#define snd_PitchSet(...)

#endif /* AUDIO_ENABLED */
