// Single quad from precomputed vertex data
void gl_QuadDrawSingle(quads *Quads, u32 index)
{
	quad *Q = &Quads->Quads[index];
	glBegin(GL_QUADS);
	glVertex2f(Q->v[0].x, Q->v[0].y);
	glVertex2f(Q->v[1].x, Q->v[1].y);
	glVertex2f(Q->v[2].x, Q->v[2].y);
	glVertex2f(Q->v[3].x, Q->v[3].y);
	glEnd();
}

// Batch rendering of quads
void gl_QuadsDraw(quads *Quads)
{
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);

	// Main quads
	glVertexPointer(2, GL_FLOAT, sizeof(quad_vert), Quads->Quads);
	glColorPointer(3, GL_FLOAT, sizeof(quad_vert), &((float *)(Quads->Quads))[2]); // offset by 2 floats for x/y
	glDrawArrays(GL_QUADS, 0, Quads->count * 4);
	glDisableClientState(GL_COLOR_ARRAY);

	// Tops
	glColor3f(GlobalBarTopColor.r, GlobalBarTopColor.g, GlobalBarTopColor.b);
	glVertexPointer(2, GL_FLOAT, 0, Quads->Top);
	glDrawArrays(GL_QUADS, 0, Quads->count * 4);
	glDisableClientState(GL_VERTEX_ARRAY);
}

// Single quad in immediate mode
void gl_DrawQuad(float x, float y, float w, float h, u32 color)
{
	float x0 = x;
	float x1 = x + w;
	float y0 = y;
	float y1 = y + h;

	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_QUADS);
	glColor4ubv((unsigned char *)&color);
	glVertex2f(x0, y0);
	glVertex2f(x0, y1);
	glVertex2f(x1, y1);
	glVertex2f(x1, y0);
	glEnd();

	glPopAttrib();
}
