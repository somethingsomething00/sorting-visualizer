#ifndef _CONFIG_H_
#define _CONFIG_H_

// Audio
// Define as 1 to enable audio support
// Currently uses OpenAL as the backend (OpenALSoft is also supported by default)
#ifndef AUDIO_ENABLED
#define AUDIO_ENABLED 1
#endif

#endif /* _CONFIG_H_ */
